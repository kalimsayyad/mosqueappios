//
//  LogoutViewController.m
//  Mosque Management
//
//  Created by Developer on 27/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "LogoutViewController.h"

@interface LogoutViewController ()

@end

@implementation LogoutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"logout called");
    
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
