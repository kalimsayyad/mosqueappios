//
//  AboutPageViewController.h
//  Mosque Management
//
//  Created by Developer on 07/02/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface AboutPageViewController : UIViewController
{
    
    MBProgressHUD *HUD; //to show the loading progress
}
@end
