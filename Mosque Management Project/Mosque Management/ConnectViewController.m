//
//  ConnectViewController.m
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "ConnectViewController.h"

@interface ConnectViewController ()

@end

@implementation ConnectViewController
@synthesize headings;

#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    headings = [NSArray arrayWithObjects:
                @"GET CONNECTED",@"CONNECT GROUPS",@"PRAYER REQUEST",@"DONATION",
                @"ISLAMIC CALENDAR",nil];
    
    
    /*UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    self.navigationController.navigationBar.topItem.leftBarButtonItem = logoutButton;*/
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar320x44.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"Connect";
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"connect page appear called");
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"connect page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [headings count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [headings objectAtIndex:indexPath.row];
    //cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ListCellBackground@2x.png"]];
    return cell;
}


/*- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init]; //creating label
    myLabel.frame = CGRectMake(20, 8, 320, 20); //asigning dimensions
    myLabel.font = [UIFont boldSystemFontOfSize:18];
    
    //titleForHeaderInSection:section returns title text
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] init]; //creating view
    headerView.backgroundColor = [UIColor grayColor];
    [headerView addSubview:myLabel]; //adding myLabel to headerView
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat result = 40;
    return result;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *heading; //heading to be returned
    
    heading =  @"News";
    return heading;
}*/


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"selected");
    /*NewsDetailViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"detailView"];
    obj.pageTitle = [headings objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:obj animated:YES];*/
    
}

#pragma mark - private methods

/*- (void) logout
 {
 NSLog(@"logout called");
 [self.tabBarController.navigationController popViewControllerAnimated:YES];
 
 //PUSH : [self.tabBarController.navigationController pushViewController:objNav animated:YES];
 
 //POP  : [self.tabBarController.navigationController popViewControllerAnimated:YES];
 
 }*/


@end
