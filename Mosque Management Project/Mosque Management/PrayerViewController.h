//
//  PrayerViewController.h
//  Mosque Management
//
//  Created by Bejoy Nair on 2/9/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrayerViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;    //to display prayer timing list

@end
