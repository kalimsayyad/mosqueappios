//
//  GalleryViewController.m
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//
#import "GalleryViewController.h"
#import "GalleryDetailViewController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#import "ModalAnimation.h"
#import "ScaleAnimation.h"

#define USER_DEFAULTS_CUSTOM_TRANSITIONS @"CustomTransitionsEnabled"
#define USER_DEFAULTS_NAVIGATION_TRANSITION @"NavigationTransition"
#define USER_DEFAULTS_NAVIGATION_TRANSITION_SLIDE @"NavigationSlide"
#define USER_DEFAULTS_NAVIGATION_TRANSITION_FLIP @"NavigationFlip"
#define USER_DEFAULTS_NAVIGATION_TRANSITION_SCALE @"NavigationScale"
//#define GALLERY_URL @"http://199.127.108.212:3008/mosque/veloziti/galleryimages"
#define GALLERY_URL @"http://192.168.1.152:3008/mosque/veloziti/galleryimages"
#define IP_ADDRESS @"http://192.168.1.152:3008/"

@interface GalleryViewController ()

@end

//static NSString *galleryUrl = @"http://192.168.1.35:3008/mosque/veloziti/galleryimages";

@implementation GalleryViewController
{
    AFHTTPRequestOperationManager *manager; //to request JSON data
    NSDictionary *galleryInfo;                 //to store JSON data
    NSArray *galleryData;
    NSString *path;
    NSString *imageUrl;
    
    ModalAnimation *_modalAnimationController;
    ScaleAnimation *_scaleAnimationController;
}
@synthesize myCollectionView;

#pragma mark - view life cycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_DEFAULTS_CUSTOM_TRANSITIONS];
    
    [[NSUserDefaults standardUserDefaults] setObject:USER_DEFAULTS_NAVIGATION_TRANSITION_SCALE forKey:USER_DEFAULTS_NAVIGATION_TRANSITION];
    
    // Initializing the HUD and adding it to the window.
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
	HUD = [[MBProgressHUD alloc] initWithWindow:window];
	
	[window addSubview:HUD];
	[HUD show: YES];
	
    HUD.labelText = @"  Loading  ";
    
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"Gallery";
    
    self.navigationController.delegate = self;
    
    
    //Load our animation controllers
    _modalAnimationController = [[ModalAnimation alloc] init];
    _scaleAnimationController = [[ScaleAnimation alloc] initWithNavigationController:self.navigationController];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _scaleAnimationController.viewForInteraction = nil;
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"gallery page appear called");
    [self fetchGalleryList];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"gallery page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods

- (void)fetchGalleryList
{
    manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSString *mosqueData = @"{\"mosque_name\":\"Fatima Meher\"}";                                   //string to be passed as parameter
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:mosqueData,@"data",nil]; //storing parameter in dictionary to be passed in url
    
    [manager GET:GALLERY_URL parameters:parameters success:^(AFHTTPRequestOperation *operation,id data)
     {
         galleryData = nil;
         galleryInfo = data;
         //NSLog(@"main data:%@",eventsInfo);
         galleryData = [galleryInfo objectForKey:@"data"];
         NSLog(@"events data:%@",galleryData);
         path = [galleryInfo objectForKey:@"path"];
         //NSLog(@"path data:%@",path);
         
         [myCollectionView reloadData];
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [HUD hide: YES];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         HUD.labelText = @"  Failed  ";
         
         [HUD hide: YES];
         
     }];
}

#pragma mark - collection view methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [galleryData count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UICollectionViewCell alloc] init];
    }
    
    //cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo-frame.png"]];
    
    NSDictionary *tempData = [galleryData objectAtIndex:indexPath.row];
    
    UIButton *galleryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [galleryButton addTarget:self action:@selector(galleryBtnClicked:) forControlEvents:UIControlEventTouchDown];
    
    imageUrl = [tempData valueForKey:@"imageUrl"];
    if (![[imageUrl substringToIndex:5] isEqualToString:@"https"]) {
        imageUrl = [NSString stringWithFormat:@"%@%@/%@",IP_ADDRESS,path,[tempData valueForKey:@"imageUrl"]];
    }
    NSLog(@"image url:%@",imageUrl);
    
    UIImageView *temp = [[UIImageView alloc] init];
    [temp setImageWithURL:[NSURL URLWithString:imageUrl]  success:^(UIImage *image, BOOL cached) {
        
        [galleryButton setBackgroundImage:temp.image forState:UIControlStateNormal];
        
        
        
    } failure:^(NSError *error) {
        
        [galleryButton setBackgroundImage:[UIImage imageNamed:@"defaultimage.png"] forState:UIControlStateNormal];
        
    }];
    
    galleryButton.frame = CGRectMake(1, 1, 98, 98);
    
    
    galleryButton.tag = indexPath.row;
    [cell addSubview:galleryButton];
    
    UILabel *picLabel = (UILabel *)[cell viewWithTag:50];
    picLabel.text = [tempData valueForKey:@"title"];;
    return cell;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    //returns the spacing between the (top,right,bottom,left).
    return UIEdgeInsetsMake(4, 4, 20, 4);
}


- (void)galleryBtnClicked:(UIButton*)sender{
    NSLog(@"Button number [%d] clicked",sender.tag);
    GalleryDetailViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"galleryDetailView"];
    NSDictionary *tempData = [galleryData objectAtIndex:sender.tag];
    
    controller.imageName = [tempData valueForKey:@"title"];
    controller.imageUrl = [NSURL URLWithString:imageUrl];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Transitioning Delegate (Modal)
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    _modalAnimationController.type = AnimationTypePresent;
    return _modalAnimationController;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    _modalAnimationController.type = AnimationTypeDismiss;
    return _modalAnimationController;
}

#pragma mark - Navigation Controller Delegate

-(id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:USER_DEFAULTS_CUSTOM_TRANSITIONS]) return nil;
    
    BaseAnimation *animationController;
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:USER_DEFAULTS_NAVIGATION_TRANSITION] isEqualToString:USER_DEFAULTS_NAVIGATION_TRANSITION_SCALE]) {
        animationController = _scaleAnimationController;
    }
    switch (operation) {
        case UINavigationControllerOperationPush:
            animationController.type = AnimationTypePresent;
            return  animationController;
        case UINavigationControllerOperationPop:
            animationController.type = AnimationTypeDismiss;
            return animationController;
        default: return nil;
    }
    
}

-(id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
    if ([animationController isKindOfClass:[ScaleAnimation class]]) {
        ScaleAnimation *controller = (ScaleAnimation *)animationController;
        if (controller.isInteractive) return controller;
        else return nil;
    } else return nil;
}

@end
