//
//  PrayerViewController.m
//  Mosque Management
//
//  Created by Bejoy Nair on 2/9/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "PrayerViewController.h"

@interface PrayerViewController (){
    
    NSArray *namazTypes;
    NSArray *namaztimings;
}

@end

@implementation PrayerViewController

#pragma mark - Life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    namazTypes = @[@"Fajr", @"Sunrise", @"Dhuhr", @"Asr", @"Maghrib", @"Isha"];
    namaztimings = @[@"06:04 AM",@"07:06 AM",@"12:49 PM",@"04:56 PM",@"06:32 PM",@"07:33 PM"];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationItem.title = @"Prayer Time";
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    UIImageView *mosqueImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 3, 38, 38)];
    mosqueImage.image = [UIImage imageNamed:@"prayer.png"];
    [cell addSubview:mosqueImage];
    
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(65, 5, 200, 32)];
    name.text = [namazTypes objectAtIndex:indexPath.row];
    name.font = [UIFont systemFontOfSize:15];
    [cell addSubview:name];
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(235, 5, 200, 32)];
    time.text = [namaztimings objectAtIndex:indexPath.row];
    time.font = [UIFont systemFontOfSize:15];
    [cell addSubview:time];
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"MMM dd, yyyy"];
    NSString *dateString = [dateFormat stringFromDate:date];
        
    UILabel *myLabel = [[UILabel alloc] init]; //creating label
    myLabel.frame = CGRectMake(0, 3, 320, 20); //asigning dimensions
    myLabel.font = [UIFont boldSystemFontOfSize:14];
    myLabel.textColor = [UIColor blackColor];
    myLabel.textAlignment = NSTextAlignmentCenter;
    
    //titleForHeaderInSection:section returns title text
    myLabel.text = dateString;
    
    UIView *headerView = [[UIView alloc] init]; //creating view
    headerView.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:myLabel]; //adding myLabel to headerView
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat result = 26;
    return result;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *heading; //heading to be returned
    if (section == 0)
    {
        heading = @"Prayer Timing";
    }
    return heading;
  
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    
    return view;
}

@end
