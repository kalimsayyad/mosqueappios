//
//  EventCellControllerCell.h
//  Mosque Management
//
//  Created by Developer on 24/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

//custom class for cells in events list
@interface EventCellController : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *month;           //to display month of event in cell
@property (weak, nonatomic) IBOutlet UILabel *date;            //to display date of event in cell
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;    //to display heading in cell
@property (weak, nonatomic) IBOutlet UILabel *subheadingLabel; //to display subheading in cell

@end
