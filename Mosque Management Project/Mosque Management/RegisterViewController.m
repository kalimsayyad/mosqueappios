//
//  RegisterViewController.m
//  Mosque Management
//
//  Created by Developer on 17/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "NewsTableViewController.h"

@interface RegisterViewController (){
    NSString *alertMessage;
    BOOL status;
}

@end

@implementation RegisterViewController
@synthesize firstNameTextField,lastNameTextField,emailTextfield,passwordTextField,confirmPasswordField,backButton;
@synthesize regBtn;

#pragma mark - life cycle functions

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstNameTextField.delegate = self;
    lastNameTextField.delegate = self;
    emailTextfield.delegate = self;
    passwordTextField.delegate = self;
    confirmPasswordField.delegate = self;
    
    regBtn.clipsToBounds = YES;
    regBtn.layer.cornerRadius = 4;
    [regBtn setBackgroundImage:[UIImage imageNamed:@"button_green.png"] forState:UIControlStateNormal];
    //[regBtn setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:200.0f/255.0f blue:223.0f/255.0f alpha:1.0f]];
    //[regBtn.layer setBorderColor: [[UIColor colorWithRed:0.0f/255.0f green:137.0f/255.0f blue:223.0f/255.0f alpha:1.0f] CGColor]];
    //[regBtn.layer setBorderWidth: 1];
    
    backButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
    [backButton setTintColor:[UIColor whiteColor]];
    
    self.navigationItem.leftBarButtonItem = backButton;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationItem.title = @"Registration";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    
    /*UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background_2.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];*/
	// Do any additional setup after loading the view.
    status = YES;
    UIColor *color = [UIColor darkGrayColor];
    firstNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter first name" attributes:@{NSForegroundColorAttributeName: color}];
    lastNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter last name" attributes:@{NSForegroundColorAttributeName: color}];
    emailTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your email address" attributes:@{NSForegroundColorAttributeName: color}];
    passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your password" attributes:@{NSForegroundColorAttributeName: color}];
    confirmPasswordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm your password" attributes:@{NSForegroundColorAttributeName: color}];
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"registration page appear called");
    [self.navigationController setNavigationBarHidden:NO];    // it shows
    //self.navigationController.navigationBar.topItem.title = @"Registration";
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"registration page disappear called");
    [self.navigationController setNavigationBarHidden:YES];    // it shows
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button functions
- (IBAction)registerButton:(id)sender {
    alertMessage = @"Please fill all the details";
    NewsTableViewController *newsTableObj = [self.storyboard instantiateViewControllerWithIdentifier:@"homeView"];
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [self.navigationController popToRootViewControllerAnimated:NO];
    [self.navigationController pushViewController:newsTableObj animated:YES];
    
    
    [UIView commitAnimations];
    /*
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:alertMessage
                                                          message:alertMessage
                                                         delegate:self
                                                cancelButtonTitle:@"Yes"
                                                otherButtonTitles:@"No",nil];
        [message show];
    }*/
    
}

- (IBAction)closeButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - textFieldDelegate functions
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //to reset position of the view
    CGRect rect = self.view.frame;
    CGFloat value = self.view.frame.origin.y;
    if  (self.view.frame.origin.y < 0)
    {
        rect.origin.y -= value;
        rect.size.height += value;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
    return YES;
}

-(void)touchesBegan:(NSSet *) touches withEvent: (UIEvent *) event
{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    
    //to reset position of the view
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect rect = self.view.frame;
    CGFloat value = self.view.frame.origin.y;
    if  (self.view.frame.origin.y < 0)
    {
        rect.origin.y -= value;
        rect.size.height += value;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect rect = self.view.frame;
    
    if ([textField isEqual:emailTextfield])
    {
        if (self.view.frame.origin.y >= 0)
        {
            rect.origin.y -= 10;
            rect.size.height += 10;
        }
        else if (self.view.frame.origin.y <= -80)
        {
            CGFloat value = 10 + self.view.frame.origin.y;
            rect.origin.y -= value;
            rect.size.height += value;
        }
        
        self.view.frame = rect;
    }
    else if ([textField isEqual:passwordTextField])
    {
        if (self.view.frame.origin.y >= 0)
        {
            rect.origin.y -= 80;
            rect.size.height += 80;
        }
        else if(self.view.frame.origin.y == -10)
        {
            rect.origin.y -= 70;
            rect.size.height += 70;
        }
        self.view.frame = rect;
    }
    else if ([textField isEqual:confirmPasswordField])
    {
        if (self.view.frame.origin.y >= 0)
        {
            rect.origin.y -= 120;
            rect.size.height += 120;
        }
        else if(self.view.frame.origin.y == -80)
        {
            rect.origin.y -= 40;
            rect.size.height += 40;
        }
        self.view.frame = rect;
    }
    else if ([textField isEqual:lastNameTextField] && self.view.frame.origin.y < 0)
    {
        CGFloat value = self.view.frame.origin.y;
        rect.origin.y -= value;
        rect.size.height += value;
        self.view.frame = rect;
    }
    [UIView commitAnimations];
    NSLog(@"(%f)",self.view.frame.origin.y);
}

-(void) textFieldDidEndEditing:(UITextField *)textField
{
    /*if ([textField isEqual:emailTextfield] && [textField.text length] == 0)
    {
        alertMessage = @"Please enter email address";
        status = NO;
    }
    if ([textField isEqual:passwordTextField] && [textField.text length] == 0)
    {
        alertMessage = @"Please enter your password";
        status = NO;
    }
    if ([textField isEqual:confirmPasswordField] && [textField.text length] == 0)
    {
        alertMessage = @"Please confirm your password";
        status = NO;
    }
    if ([textField isEqual:lastNameTextField] && [textField.text length] == 0)
    {
        alertMessage = @"Please enter your username";
        status = NO;
    }*/
}

#pragma mark - private methods
-(void) closeView{
    NSLog(@"close view called");
    LoginViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    [self.navigationController pushViewController:obj animated:YES];
    [UIView commitAnimations];
}

@end
