//
//  MessageViewController.m
//  Mosque Management
//
//  Created by Developer on 21/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "MessageViewController.h"
#import "MessageDetailsViewController.h"
#import "AudioMessageViewController.h"
#import "MessagesCellController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#define MESSAGES_URL @"http://192.168.1.152:3008/mosque/veloziti/messages"
#define IP_ADDRESS @"http://192.168.1.152:3008/"

@interface MessageViewController (){
    AFHTTPRequestOperationManager *manager; //to request JSON data
    NSDictionary *messagesInfo;                 //to store JSON data
    NSArray *messagesData;
    NSString *path;
    NSMutableArray *podcastMessages;        //to store podcast messages
    NSMutableArray *videoMessages;          //to store video messages
}


@end

//static NSString *messagesUrl = @"http://192.168.1.35:3008/mosque/veloziti/messages";

@implementation MessageViewController
@synthesize messageType,messageTable;

#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Initializing the HUD and adding it to the window.
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
	HUD = [[MBProgressHUD alloc] initWithWindow:window];
	
	[window addSubview:HUD];
	[HUD show: YES];
    
    [self fetchMessagesList];
	
    HUD.labelText = @"  Loading  ";
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"Messages";
    type = @"audio";
    
    [messageType setBackgroundImage:[UIImage imageNamed:@"button_green.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [messageType setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    messageType.layer.cornerRadius = 6;
    messageType.layer.masksToBounds = YES;
    messageType.tintColor = [UIColor whiteColor];
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"message page appear called");
    //block the reload table
    UITableViewController *messagesTableController = [[UITableViewController alloc] init];
    messagesTableController.tableView = messageTable;
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    messagesTableController.refreshControl = refresh;
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"message page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - segmented control methods
- (IBAction) segmentedControlIndexChanged
{
    switch (messageType.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"Segment 1 selected.");
            type = @"audio";
            [messageTable reloadData];
            
            break;
        case 1:
            NSLog(@"Segment 2 selected.");
            type = @"video";
            [messageTable reloadData];
            break;
    }
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([type isEqualToString:@"audio"]) {
        NSLog(@"selected array count: %d",[podcastMessages count]);
        return [podcastMessages count];
    }
    else{ //if([type isEqualToString:@"video"]){
        NSLog(@"selected array count: %d",[videoMessages count]);
        return [videoMessages count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    MessagesCellController *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MessagesCellController alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.subHeadingLabel.numberOfLines = 2;
    [cell.subHeadingLabel sizeToFit];
    
    if ([type isEqualToString:@"audio"]) {
        //cell.cellImage.image = [UIImage imageNamed:@"podcaster.png"];
        NSDictionary *currentMessage = [podcastMessages objectAtIndex:indexPath.row];
        cell.headingLabel.text = [currentMessage valueForKey:@"title"];
        cell.subHeadingLabel.text = [currentMessage valueForKey:@"shortdescription"];
        
        NSString *imageUrl = [currentMessage valueForKey:@"imageUrl"];
        if (![[imageUrl substringToIndex:5] isEqualToString:@"https"]) {
            imageUrl = [NSString stringWithFormat:@"%@%@/%@",IP_ADDRESS,path,[currentMessage valueForKey:@"imageUrl"]];
        }
        NSLog(@"image url:%@",imageUrl);
        
        UIImageView *temp = [[UIImageView alloc] init];
        
        [temp setImageWithURL:[NSURL URLWithString:[currentMessage valueForKey:@"imageUrl"]]  success:^(UIImage *image, BOOL cached) {
            
            cell.cellImage.image = temp.image;
            
            
            
        } failure:^(NSError *error) {
            
            cell.cellImage.image= [UIImage imageNamed:@"defaultimage.png"];
            
        }];
    }
    else{
        //cell.cellImage.image = [UIImage imageNamed:@"videocaster.png"];
        NSDictionary *currentMessage = [videoMessages objectAtIndex:indexPath.row];
        cell.headingLabel.text = [currentMessage valueForKey:@"title"];
        cell.subHeadingLabel.text = [currentMessage valueForKey:@"shortdescription"];
        
        NSString *imageUrl = [currentMessage valueForKey:@"imageUrl"];
        if (![[imageUrl substringToIndex:5] isEqualToString:@"https"]) {
            imageUrl = [NSString stringWithFormat:@"%@%@/%@",IP_ADDRESS,path,[currentMessage valueForKey:@"imageUrl"]];
        }
        NSLog(@"image url:%@",imageUrl);
        
        UIImageView *temp = [[UIImageView alloc] init];
        
        [temp setImageWithURL:[NSURL URLWithString:imageUrl]  success:^(UIImage *image, BOOL cached) {
            
            cell.cellImage.image = temp.image;
            
            
            
        } failure:^(NSError *error) {
            
            cell.cellImage.image = [UIImage imageNamed:@"defaultimage.png"];
            
        }];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"selected");
    
    //obj.pageTitle = [selectedArray objectAtIndex:indexPath.row];
    NSDictionary *currentMessage = [[NSDictionary alloc] init];
    if ([type isEqualToString:@"audio"]) {
        currentMessage = [podcastMessages objectAtIndex:indexPath.row];
        AudioMessageViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"audioView"];
        obj.currentData = currentMessage;
        [self.navigationController pushViewController:obj animated:YES];
    }
    else{
        currentMessage = [videoMessages objectAtIndex:indexPath.row];
        MessageDetailsViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"messageDetailView"];
        obj.currentData = currentMessage;
        [self.navigationController pushViewController:obj animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72.0;
}

#pragma mark - private methods

//function to reload table
-(void)refreshView:(UIRefreshControl *)refresh
{
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    [self fetchMessagesList];
    
    [messageTable reloadData];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@",[formatter stringFromDate:[NSDate date]]];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}

- (void)fetchMessagesList
{
    podcastMessages = [[NSMutableArray alloc] init];
    videoMessages = [[NSMutableArray alloc] init];
    
    manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSString *mosqueData = @"{\"mosque_name\":\"Fatima Meher\"}";                                   //string to be passed as parameter
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:mosqueData,@"data",nil]; //storing parameter in dictionary to be passed in url
    
    [manager GET:MESSAGES_URL parameters:parameters success:^(AFHTTPRequestOperation *operation,id data)
     {
         messagesInfo = data;
         NSLog(@"main data:%@",messagesInfo);
         messagesData = [messagesInfo objectForKey:@"data"];
         NSLog(@"news data:%@",messagesData);
         path = [messagesInfo objectForKey:@"path"];
         NSLog(@"path data:%@",path);

         
         for(id message in messagesData)
         {
             NSDictionary *currentMessage = message;
             if([[currentMessage objectForKey:@"type"] isEqualToString:@"Podcast"])
             {
                 NSLog(@"audio is added");
                 [podcastMessages addObject:currentMessage];
             }
             else
             {
                 [videoMessages addObject:currentMessage];
             }
         }
         
         [messageTable reloadData];
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [HUD hide: YES];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         HUD.labelText = @"  Failed  ";
         
         [HUD hide: YES];
         
     }];
}

@end
