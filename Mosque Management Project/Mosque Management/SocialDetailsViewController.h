//
//  SocialDetailsViewController.h
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialDetailsViewController : UIViewController<UIWebViewDelegate>
{
    NSString *fullUrl;                      //to store url(currently not used)
    UIActivityIndicatorView *activityView;  //to show loading activity

}

@property (weak, nonatomic) IBOutlet UIWebView *webView;    //to load the webpage
@property (weak, nonatomic) NSString *website;              //to store the website name

@end
