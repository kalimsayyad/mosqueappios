//
//  MessageDetailsViewController.h
//  Mosque Management
//
//  Created by Developer on 21/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MessageDetailsViewController : UIViewController <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

/*UIActionSheetDelegate: to implement action sheet
 MFMailComposeViewControllerDelegate: to implement email functionality
 MFMessageComposeViewControllerDelegate: to implement message functionality
 UIWebViewDelegate: to use webview delegate methods
 */

@property (weak, nonatomic) NSDictionary *currentData;           //to store current message data
@property (strong, nonatomic) UIBarButtonItem *shareButton;      //for share button
@property (weak, nonatomic) IBOutlet UIButton *videoButton;      //to show video image & play option
@property (weak, nonatomic) IBOutlet UITextView *messageDetails; //to show message details
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;       //to show author name
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;         //to show date of message

@end
