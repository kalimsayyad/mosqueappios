//
//  EventsTableViewController.h
//  Mosque Management
//
//  Created by Developer on 27/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface EventsTableViewController : UITableViewController{
    
    MBProgressHUD *HUD; //to show the loading progress
}

@end
