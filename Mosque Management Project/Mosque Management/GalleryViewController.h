//
//  GalleryViewController.h
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GalleryDetailViewController.h"
#import "MBProgressHUD.h"

@interface GalleryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIViewControllerTransitioningDelegate, UINavigationControllerDelegate, UITabBarControllerDelegate>

/*UIViewControllerTransitioningDelegate: to use controller transition delegate methods
 UINavigationControllerDelegate: to use navigation controller delegate methods
 UITabBarControllerDelegate: to use tab bar controller delegate methods
 */

{
    MBProgressHUD *HUD; //to show the loading progress

}
@property (strong,nonatomic) IBOutlet UICollectionView *myCollectionView; //view to display images

@end
