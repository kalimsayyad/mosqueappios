//
//  EventsDetailViewController.m
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "EventsDetailViewController.h"
#import <Social/Social.h>
#define EVENTS_URL @"http://192.168.1.152:3008/mosque/veloziti/events"
#define IP_ADDRESS @"http://192.168.1.152:3008/"


@interface EventsDetailViewController ()

@end

@implementation EventsDetailViewController
{
    UISegmentedControl *decisionButton; //decision button(join/maybe/decline)
}
@synthesize shareButton,eventData,path;


#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareButton = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(displayActionSheet:)];
    
    self.navigationItem.rightBarButtonItem = shareButton;
    self.navigationItem.title = @"Event Details";
    
    [self setLayout];
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"event detail page appear called");
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"event detail page disappear called");
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods

-(void)setLayout
{
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    UIImageView *eventImage = [[UIImageView alloc] initWithFrame:CGRectMake(4, 68, 312, 142)];
    
    NSString *imageUrl = [eventData valueForKey:@"imageUrl"];
    if (![[imageUrl substringToIndex:5] isEqualToString:@"https"]) {
        imageUrl = [NSString stringWithFormat:@"%@%@/%@",IP_ADDRESS,path,[eventData valueForKey:@"imageUrl"]];
    }
    NSLog(@"image url:%@",imageUrl);
    
    eventImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:(NSString *)[eventData valueForKey:@"imageUrl"]]]];
    
    decisionButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Join",@"May Be",@"Decline", nil]];
    decisionButton.frame = CGRectMake(4, 214, 312, 29);
    decisionButton.selectedSegmentIndex = -1;
    decisionButton.tintColor = [UIColor whiteColor];
    [decisionButton setBackgroundImage:[UIImage imageNamed:@"button_green.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [decisionButton setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    decisionButton.layer.cornerRadius = 6;
    decisionButton.layer.masksToBounds = YES;
    [decisionButton addTarget:self action:@selector(decisionChanged:) forControlEvents: UIControlEventValueChanged];
    
    MKMapView *eventMap = [[MKMapView alloc] initWithFrame:CGRectMake(4, 248, 312, 142)];
    [eventMap.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [eventMap.layer setBorderWidth:1];
    
    eventMap.mapType = MKMapTypeHybrid;
    
    MKCoordinateRegion region;
    region.center=eventMap.region.center;
    region.span.latitudeDelta = 0.007500;
    region.span.longitudeDelta = 0.007500;
    region.center.latitude = 24.412999;
    region.center.longitude = 54.474489;
    /*region.center.latitude = [[eventData valueForKey:@"latitude"] doubleValue];
    region.center.longitude = [[eventData valueForKey:@"longitude"] doubleValue];*/
    
    CLLocationCoordinate2D annotationCoord;
    annotationCoord.latitude = 24.412999;
    annotationCoord.longitude = 54.474489;
    /*annotationCoord.latitude = [[eventData valueForKey:@"latitude"] doubleValue];
    annotationCoord.longitude = [[eventData valueForKey:@"longitude"] doubleValue];*/
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = [eventData valueForKey:@"title"];//@"Shaikh Zayad Grand Mosque";
    annotationPoint.subtitle = [eventData valueForKey:@"venue"];//@"Abu Dhabi";
    [eventMap addAnnotation:annotationPoint];
    [eventMap setRegion:region animated:TRUE];
    
    CGFloat titleY = eventMap.frame.origin.y + eventMap.frame.size.height + 5;
    
    UILabel *eventTitle = [[UILabel alloc] initWithFrame:CGRectMake( 4 , titleY , 312 , 21)];
    eventTitle.font = [UIFont boldSystemFontOfSize:14];
    eventTitle.textAlignment = NSTextAlignmentCenter;
    eventTitle.text = [eventData valueForKey:@"title"];
    
    CGFloat dateY = eventTitle.frame.origin.y + eventTitle.frame.size.height + 5;
    
    UILabel *eventDate = [[UILabel alloc] initWithFrame:CGRectMake(4, dateY, 156, 21)];
    eventDate.font = [UIFont systemFontOfSize:14];
    eventDate.text = [NSString stringWithFormat:@"Date: %@",[eventData valueForKey:@"eventdate"]];
    
    CGFloat timeX = eventDate.frame.origin.x + eventDate.frame.size.width;
    
    UILabel *eventTime = [[UILabel alloc] initWithFrame:CGRectMake(timeX, dateY, 156, 21)];
    eventTime.font = [UIFont systemFontOfSize:14];
    eventTime.textAlignment = NSTextAlignmentRight;
    eventTime.text = [NSString stringWithFormat:@"Time: %@",[eventData valueForKey:@"eventtime"]];
    
    CGFloat venueY = eventDate.frame.origin.y + eventDate.frame.size.height;
    
    UILabel *eventVenue = [[UILabel alloc] initWithFrame:CGRectMake( 4 , venueY , 312 , 21)];
    eventVenue.font = [UIFont systemFontOfSize:14];
    eventVenue.textAlignment = NSTextAlignmentCenter;
    eventVenue.text = [NSString stringWithFormat:@"Venue:%@",[eventData valueForKey:@"venue"]];
    
    CGFloat detailsY = eventVenue.frame.origin.y + eventVenue.frame.size.height + 5;
    
    UILabel *eventDetails = [[UILabel alloc] initWithFrame:CGRectMake(4, detailsY, 312, 164)];
    eventDetails.font = [UIFont systemFontOfSize:14];
    eventDetails.textAlignment = NSTextAlignmentJustified;
    eventDetails.text = [eventData valueForKey:@"description"];
    eventDetails.numberOfLines = 0;
    [eventDetails sizeToFit];
    
    [scroll addSubview:eventImage];
    [scroll addSubview:decisionButton];
    [scroll addSubview:eventMap];
    [scroll addSubview:eventTitle];
    [scroll addSubview:eventDate];
    [scroll addSubview:eventTime];
    [scroll addSubview:eventVenue];
    [scroll addSubview:eventDetails];
    
    CGFloat scrollHeight = eventDetails.frame.origin.y + eventDetails.frame.size.height + 60;
    scroll.contentSize = CGSizeMake(self.view.frame.size.width,scrollHeight);
    [self.view addSubview:scroll];
}

#pragma mark - action sheet methods
- (IBAction)displayActionSheet:(id)sender
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share Event" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Mail", @"Message", @"Facebook", @"Twitter", nil];
        
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showFromBarButtonItem:shareButton animated:YES];
    }
    
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
    {
        switch (buttonIndex)
        {
            case 0:
            {
                NSLog(@"email button clicked.");
                // Email Subject
                NSString *emailTitle = @"Test Email";
                // Email Content
                NSString *messageBody = @"iOS programming is so fun!";
                // To address
                NSArray *toRecipents = [NSArray arrayWithObject:@"anand.bhunia@veloziti.com"];
                
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                mc.mailComposeDelegate = self;
                [mc setSubject:emailTitle];
                [mc setMessageBody:messageBody isHTML:NO];
                [mc setToRecipients:toRecipents];
                
                // Present mail view controller on screen
                [self presentViewController:mc animated:YES completion:NULL];
            }
            break;
            
            case 1:
            {
                NSLog(@"sms button clicked.");
                NSString *message = [NSString stringWithFormat:@"%@ on %@ at %@, venue:%@",[eventData valueForKey:@"title"],[eventData valueForKey:@"date"],[eventData valueForKey:@"time"],[eventData valueForKey:@"venue"]];
                [self showSMS:message];
            }
                break;
                
            case 2:
                
            {
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
                {
                    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                    [tweetSheet setInitialText:[NSString stringWithFormat:@"%@ \n link : %@",[eventData valueForKey:@"title"],[eventData valueForKey:@"webPage"]]];
                    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
                    {
                        switch(result)
                        {
                                //  This means the user cancelled without sending the Tweet
                            case SLComposeViewControllerResultCancelled:
                            {
                                UIAlertView *alertViewCancel = [[UIAlertView alloc]
                                                                initWithTitle:@"Sorry"
                                                                message:@"Status is not Updated"
                                                                delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                                [alertViewCancel show];
                                break;
                            }
                                //  This means the user hit 'Send'
                            case SLComposeViewControllerResultDone:
                            {
                                UIAlertView *alertViewDone = [[UIAlertView alloc]
                                                              initWithTitle:@"Success"
                                                              message:@"Status is Update"
                                                              delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                                [alertViewDone show];
                                break;
                            }
                                break;
                        }
                    };
                    
                    
                    
                    [self presentViewController:tweetSheet animated:YES completion:nil];
                    
                }
                else
                {
                    UIAlertView *alertView = [[UIAlertView alloc]
                                              initWithTitle:@"Sorry"
                                              message:@"You can't update status right now, make sure your device has an internet connection and you have at least one facebook account setup"
                                              delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }
                
                
                
                break;
                
            case 3:
            {
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
                {
                    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    [tweetSheet setInitialText:[NSString stringWithFormat:@"%@ \n link : %@",[eventData valueForKey:@"title"],[eventData valueForKey:@"webPage"]]];
                    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
                    {
                        switch(result)
                        {
                                //  This means the user cancelled without sending the Tweet
                            case SLComposeViewControllerResultCancelled:
                            {
                                UIAlertView *alertViewCancel = [[UIAlertView alloc]
                                                                initWithTitle:@"Sorry"
                                                                message:@"Twitt is not post"
                                                                delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                                [alertViewCancel show];
                                break;
                            }
                                //  This means the user hit 'Send'
                            case SLComposeViewControllerResultDone:
                            {
                                UIAlertView *alertViewDone = [[UIAlertView alloc]
                                                              initWithTitle:@"Success"
                                                              message:@"Twitt is post"
                                                              delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                                [alertViewDone show];
                                break;
                            }
                                break;
                        }
                    };
                    
                    
                    
                    [self presentViewController:tweetSheet animated:YES completion:nil];
                    
                }
                else
                {
                    UIAlertView *alertView = [[UIAlertView alloc]
                                              initWithTitle:@"Sorry"
                                              message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                              delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }

                break;
        }
    }
    
    
#pragma mark - mail composition methods
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
    {
        switch (result)
        {
            case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
            case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
            case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
            case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
            default:
            break;
        }
        
        // Close the Mail Interface
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
#pragma mark - SMS composition methods
- (void)showSMS:(NSString*)messageText {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[@"12345678", @"72345524"];
    NSString *message = [NSString stringWithFormat:@"Just sent the %@ file to your email. Please check!", messageText];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}
    
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
    {
        switch (result) {
            case MessageComposeResultCancelled:
            break;
            
            case MessageComposeResultFailed:
            {
                UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [warningAlert show];
                break;
            }
            
            case MessageComposeResultSent:
            break;
            
            default:
            break;
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }


#pragma mark - segmented control methods
- (IBAction)decisionChanged:(id)sender
{
    NSString *alertMessage = @"Error occured";
    switch (decisionButton.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"Segment 1 selected.");
            alertMessage = @"Your Join request sent successfully.";
            break;
        case 1:
             NSLog(@"Segment 2 selected.");
            alertMessage = @"Your Maybe request sent successfully.";
             break;
        case 2:
             NSLog(@"Segment 3 selected.");
            alertMessage = @"Your Decline request sent successfully.";
             break;
    }
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:alertMessage
                                                      message:alertMessage
                                                     delegate:self
                                            cancelButtonTitle:@"Ok"
                                            otherButtonTitles:nil];
    [message show];
}
@end
