//
//  GalleryDetailViewController.h
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface GalleryDetailViewController : UIViewController <UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageContainer;
@property (weak, nonatomic) NSString *imageName;
@property (weak, nonatomic) NSURL *imageUrl;
@property (strong, nonatomic) UIBarButtonItem *backButton;
@property (strong, nonatomic) UIBarButtonItem *shareButton;
@end
