//
//  ConnectViewController.h
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) NSArray *headings;

@end
