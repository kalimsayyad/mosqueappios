//
//  EventsViewController.m
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "EventsViewController.h"
#import "EventsDetailViewController.h"
#import "EventCellController.h"

@interface EventsViewController ()

@end

@implementation EventsViewController
@synthesize upcomingEvents,pastEvents,subHeadings1,subHeading2,pastDates,pastMonths,upComeDates,upComeMonths;

- (void)viewDidLoad
{
    [super viewDidLoad];
    upcomingEvents = [NSArray arrayWithObjects:
                @"Title1",@"Title2",@"Title3",@"Title4",
                      @"Title5",@"Title6",nil];
    pastEvents = [NSArray arrayWithObjects:@"Title7",@"Title8",
                @"Title9",@"Title10",@"Title11",@"Title12",nil];
    
    subHeadings1 = [NSArray arrayWithObjects:
                   @"Detail about cell 1",@"Detail about cell 2",@"Detail about cell 3",@"Detail about cell 4",
                    @"Detail about cell 5",@"Detail about cell 6",nil];
    subHeading2 = [NSArray arrayWithObjects:@"Detail about cell 7",@"Detail about cell 8",
                   @"Detail about cell 9",@"Detail about cell 10",@"Detail about cell 11",@"Detail about cell 12",nil];
    
    upComeDates = [NSArray arrayWithObjects:@"27",@"30",@"31",@"4",@"8",@"10",nil];
    upComeMonths =[NSArray arrayWithObjects:@"JAN",@"JAN",@"JAN",@"FEB",@"FEB",@"FEB",nil];
    
    pastDates = [NSArray arrayWithObjects:@"15",@"18",@"28",@"12",@"18",@"20",nil];
    pastMonths =[NSArray arrayWithObjects:@"DEC",@"DEC",@"DEC",@"JAN",@"JAN",@"JAN",nil];
    
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    self.navigationController.navigationBar.topItem.leftBarButtonItem = logoutButton;
    self.navigationController.navigationBar.topItem.title = @"Events";
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"events page appear called");
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"events page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int count;
    if (section == 0) {
        count = [upcomingEvents count];
    }
    else
    {
        count = [pastEvents count];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"customCell";
    
     EventCellController *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[EventCellController alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    if (indexPath.section == 0) {
        cell.headingLabel.text = [upcomingEvents objectAtIndex:indexPath.row];
        cell.subheadingLabel.text = [subHeadings1 objectAtIndex:indexPath.row];
        cell.month.text = [upComeMonths objectAtIndex:indexPath.row];
        cell.date.text = [upComeDates objectAtIndex:indexPath.row];
    }
    else
    {
        cell.headingLabel.text = [pastEvents objectAtIndex:indexPath.row];
        cell.subheadingLabel.text = [subHeading2 objectAtIndex:indexPath.row];
        cell.month.text = [pastMonths objectAtIndex:indexPath.row];
        cell.date.text = [pastDates objectAtIndex:indexPath.row];
    }
    
    cell.month.layer.cornerRadius = 5.0;
    [cell.month.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [cell.month.layer setBorderWidth:2];
    cell.date.layer.cornerRadius = 5.0;
    [cell.date.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [cell.date.layer setBorderWidth:2];

    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init]; //creating label
    myLabel.frame = CGRectMake(20, 8, 320, 20); //asigning dimensions
    myLabel.font = [UIFont boldSystemFontOfSize:18];
    myLabel.textColor = [UIColor grayColor];
    
    //titleForHeaderInSection:section returns title text
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] init]; //creating view
    headerView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    [headerView addSubview:myLabel]; //adding myLabel to headerView
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat result = 40;
    return result;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *heading; //heading to be returned
    if (section == 0) {
        heading = @"Upcoming Events";
    }
    else
    {
        heading =  @"Past Events";
    }
    return heading;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"selected");
    EventsDetailViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"eventDetailView"];
    if (indexPath.section == 0) {
        
    }
    else
    {
        
    }
    
    [self.navigationController pushViewController:obj animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}

#pragma mark - private methods

- (void) logout
{
    NSLog(@"logout called");
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
    
    /*PUSH : [self.tabBarController.navigationController pushViewController:objNav animated:YES];
     
     POP  : [self.tabBarController.navigationController popViewControllerAnimated:YES];*/
    
}

@end
