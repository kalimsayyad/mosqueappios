//
//  MapViewController.h
//  Mosque Management
//
//  Created by Syed Adnan on 07/02/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController <CLLocationManagerDelegate>
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (strong, nonatomic) UIBarButtonItem *backButton;
@property (assign, nonatomic) float mosqueLatitude;
@property (assign, nonatomic) float mosqueLongitude;
@property (weak, nonatomic) NSString *mosqueName;
@property (weak, nonatomic) NSString *mosqueAddress;

@end
