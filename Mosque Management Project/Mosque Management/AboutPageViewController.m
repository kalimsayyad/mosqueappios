//
//  AboutPageViewController.m
//  Mosque Management
//
//  Created by Developer on 07/02/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "AboutPageViewController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "MapViewController.h"
#define ABOUT_URL @"http://199.127.108.212:3008/mosque/veloziti/mosque"

@interface AboutPageViewController (){
    AFHTTPRequestOperationManager *manager; //to request JSON data
    NSArray *mosqueData;                       //to store JSON data
    NSDictionary *mosqueInfo;
}


@end

//static NSString *aboutUrl = @"https://dl.dropboxusercontent.com/u/65857105/gallery/Mosque/MosqueAbout.json";

@implementation AboutPageViewController

#pragma mark - Life Cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mosqueInfo = [[NSDictionary alloc] init];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
	[HUD show: YES];
	
    HUD.labelText = @"  Loading  ";
    [self fetchAboutList];
    
    self.navigationItem.title = @"About Mosque";
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)fetchAboutList{
    NSLog(@"fetch function called");
    manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSString *mosqueName = @"{\"mosque_name\":\"Fatima Meher\"}";                                    //string to be passed as parameter
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:mosqueName,@"data",nil]; //storing parameter in dictionary to be passed in url
    
    [manager GET:ABOUT_URL parameters:parameters success:^(AFHTTPRequestOperation *operation,id data)
     {
         /*NSDictionary * myNewData = data;
         mosqueData = [myNewData valueForKey:@"About"];*/
         
         mosqueData = data;
         mosqueInfo  = [mosqueData objectAtIndex:0];
         
         [self fillData];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [HUD hide: YES];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         HUD.labelText = @"  Failed  ";
         
         [HUD hide: YES];
         
     }];
    
}

-(void)fillData
{
    UIScrollView *aboutUsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];

    /*NSDictionary *imamDetails = [mosqueInfo valueForKey:@"imam"];
    NSLog(@"imam details:%@",imamDetails);*/
    
    self.navigationItem.title = [mosqueInfo valueForKey:@"mosquename"]; //setting navigation bar title
    
    //creating imam section
    UILabel *imamSection = [[UILabel alloc] initWithFrame:CGRectMake(0, 65, 320, 25)];
    imamSection.font = [UIFont boldSystemFontOfSize:14];
    imamSection.textColor = [UIColor blackColor];
    imamSection.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    imamSection.text = @"   Imam";
    
    //setting Y co-ordinate for mosque imageview
    CGFloat imageY = imamSection.frame.origin.y + imamSection.frame.size.height + 2;
    
    //creating imageview to display mosque image
    UIImageView *mosqueImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, imageY,88, 110)];
    //mosqueImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:(NSString *)[imamDetails valueForKey:@"imageUrl"]]]];
    
   /* UILabel *imam = [[UILabel alloc] initWithFrame:CGRectMake(100, imageY, 48, 21)];
    imam.font = [UIFont boldSystemFontOfSize:14];
    imam.text = @"Imam:";*/
    
    //CGFloat nameY = imageY+10;//imam.frame.origin.y + imam.frame.size.height;
    
    //creating label to display imam name
    UILabel *imamName = [[UILabel alloc] initWithFrame:CGRectMake(100, imageY, 221, 21)];
    imamName.font = [UIFont boldSystemFontOfSize:15];
    imamName.text = [mosqueInfo valueForKey:@"imamname"];
    
    //setting Y co-ordinate for about imam label
    CGFloat aboutImamLY = imamName.frame.origin.y + imamName.frame.size.height + 3;
    
    //creating about imam label
    UILabel *aboutImamLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, aboutImamLY, 100, 14)];
    aboutImamLabel.font = [UIFont systemFontOfSize:14];
    aboutImamLabel.text = @"About Imam :";
    
    //setting Y co-ordinate for imam description
    CGFloat iDescY = aboutImamLabel.frame.origin.y + aboutImamLabel.frame.size.height + 2;
    
    //creating label to display imam description
    UILabel *imamDescription = [[UILabel alloc] initWithFrame:CGRectMake(100, iDescY, 220, 65)];
    imamDescription.font = [UIFont systemFontOfSize:13];
    imamDescription.numberOfLines = 0;
    imamDescription.text = [mosqueInfo valueForKey:@"imamdescription"];
    [imamDescription sizeToFit];
    
    //setting Y co-ordinate for vision section
    CGFloat visionY = imamDescription.frame.origin.y + imamDescription.frame.size.height + 16;
    
    //creating vision section
    UILabel *visionSection = [[UILabel alloc] initWithFrame:CGRectMake(0, visionY, 320, 25)];
    visionSection.font = [UIFont boldSystemFontOfSize:14];
    visionSection.textColor = [UIColor blackColor];
    visionSection.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    visionSection.text = @"   Vision";
    
    //setting Y co-ordinate for vision label
    CGFloat visionLY = visionSection.frame.origin.y + visionSection.frame.size.height + 2;
    
    //creating label to display vision
    UILabel *visionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, visionLY, 304, 22)];
    visionLabel.font = [UIFont systemFontOfSize:14];
    visionLabel.text = [mosqueInfo valueForKey:@"vision"];
    visionLabel.numberOfLines = 0;
    [visionLabel sizeToFit];
    
    //setting Y co-ordinate for description section
    CGFloat descY = visionLabel.frame.origin.y + visionLabel.frame.size.height + 5;
    
    //creating description section
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake( 0, descY , 320 , 25)];
    description.font = [UIFont boldSystemFontOfSize:14];
    description.textColor = [UIColor blackColor];
    description.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    description.text = @"   Description";
    
    //setting Y co-ordinate for description label
    CGFloat descLY = description.frame.origin.y + description.frame.size.height + 5;
    
    //creating label to display description
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, descLY, 305, 22)];
    descriptionLabel.font = [UIFont systemFontOfSize:14];
    descriptionLabel.text = [mosqueInfo valueForKey:@"mosquedescription"];
    descriptionLabel.numberOfLines = 0;
    [descriptionLabel sizeToFit];
    
    //setting Y co-ordinate for description label
    CGFloat contactY = descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + 5;
    
    //creating contact section
    UILabel *contactLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contactY , 320 , 25)];
    contactLabel.font = [UIFont boldSystemFontOfSize:14];
    contactLabel.textColor = [UIColor blackColor];
    contactLabel.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    contactLabel.text = @"   Contact:";
    
    //setting Y co-ordinate for label address
    CGFloat addressY = contactLabel.frame.origin.y + contactLabel.frame.size.height;
    
    //creating label address
    UILabel *address = [[UILabel alloc] initWithFrame:CGRectMake( 5 , addressY , 200 , 21)];
    address.font = [UIFont systemFontOfSize:14];
    address.text = @"Address:";
    
    //setting Y co-ordinate for address label
    CGFloat addressLY = address.frame.origin.y + address.frame.size.height;
    
    //creating label to display address
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, addressLY, 309, 22)];
    addressLabel.font = [UIFont systemFontOfSize:14];
    addressLabel.text = [mosqueInfo valueForKey:@"mosqueaddress"];
    addressLabel.numberOfLines = 0;
    [addressLabel sizeToFit];
    
    //setting Y co-ordinate for label phone number
    CGFloat phoneY = addressLabel.frame.origin.y + addressLabel.frame.size.height;
    
    //creating label phone number
    UILabel *phone = [[UILabel alloc] initWithFrame:CGRectMake( 5 , phoneY , 101 , 21)];
    phone.font = [UIFont systemFontOfSize:14];
    phone.text = @"Phone number:";
    
    //setting X co-ordinate for contact number label
    CGFloat phoneLX = phone.frame.origin.x + phone.frame.size.width;
    
    //creating label to display contact number
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake( phoneLX +10 , phoneY , 190 , 21)];
    phoneLabel.font = [UIFont systemFontOfSize:14];
    phoneLabel.text = [mosqueInfo valueForKey:@"mosquecontact"];
    
    //setting Y co-ordinate for map button
    CGFloat buttonY = phone.frame.origin.y + phone.frame.size.height + 5;
    
    //creating map button
    UIButton *mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mapButton addTarget:self action:@selector(showMap) forControlEvents:UIControlEventTouchDown];
    [mapButton setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forState:UIControlStateNormal];
    [mapButton setTitle:@"View Map" forState:UIControlStateNormal];
    mapButton.frame = CGRectMake(5, buttonY, 310, 40);

    //adding all elements to scrollview
    [aboutUsScrollView addSubview:imamSection];
    [aboutUsScrollView addSubview:mosqueImage];
    //[aboutUsScrollView addSubview:imam];
    [aboutUsScrollView addSubview:imamName];
    [aboutUsScrollView addSubview:aboutImamLabel];
    [aboutUsScrollView addSubview:imamDescription];
    [aboutUsScrollView addSubview:visionSection];
    [aboutUsScrollView addSubview:visionLabel];
    [aboutUsScrollView addSubview:description];
    [aboutUsScrollView addSubview:descriptionLabel];
    [aboutUsScrollView addSubview:contactLabel];
    [aboutUsScrollView addSubview:address];
    [aboutUsScrollView addSubview:addressLabel];
    [aboutUsScrollView addSubview:phone];
    [aboutUsScrollView addSubview:phoneLabel];
    [aboutUsScrollView addSubview:mapButton];
    
    //setting the height of scroll view
    CGFloat scrollHeight = mapButton.frame.origin.y + mapButton.frame.size.height + 20;
    
    aboutUsScrollView.contentSize = CGSizeMake(self.view.frame.size.width,scrollHeight);
    [self.view addSubview:aboutUsScrollView];

}

-(void) showMap
{
    NSLog(@"latitude:%@ longitude:%@",[mosqueInfo valueForKey:@"latitude"],[mosqueInfo valueForKey:@"longitude"]);

    MapViewController *mapviewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"MapView"];
    mapviewcontroller.mosqueLatitude = 18.51663;//[[mosqueInfo valueForKey:@"latitude"] floatValue];//18.51663;
    mapviewcontroller.mosqueLongitude = 73.86483;//[[mosqueInfo valueForKey:@"longitude"] floatValue];//73.86483;
    mapviewcontroller.mosqueName = [mosqueInfo valueForKey:@"mosquename"];
    mapviewcontroller.mosqueAddress = [mosqueInfo valueForKey:@"mosqueaddress"];
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:mapviewcontroller];
    
    [self presentViewController:controller animated:YES completion:nil];

}

@end
