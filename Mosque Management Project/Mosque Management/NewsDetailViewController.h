//
//  NewsDetailViewController.h
//  Mosque Management
//
//  Created by Developer on 20/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface NewsDetailViewController : UIViewController <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UIWebViewDelegate>

/*UIActionSheetDelegate: to implement action sheet
 MFMailComposeViewControllerDelegate: to implement email functionality
 MFMessageComposeViewControllerDelegate: to implement message functionality
 UIWebViewDelegate: to use webview delegate methods
 */

{
    UIActivityIndicatorView *activityView;                  //to show loading of webpage
}
@property (weak, nonatomic) NSDictionary *pageData;         //to store news data
@property (weak, nonatomic) IBOutlet UIWebView *webView;    //to display webpage
@property (strong, nonatomic) UIBarButtonItem *shareButton; //for share button

@end
