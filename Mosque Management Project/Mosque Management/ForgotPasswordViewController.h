//
//  ForgotPasswordViewController.h
//  Mosque Management
//
//  Created by Developer on 17/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController <UITextFieldDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *firstAnsTextField;
@property (weak, nonatomic) IBOutlet UITextField *secondAnsTextField;
@property (weak, nonatomic) IBOutlet UITextField *thirdAnsTextField;
- (IBAction)submitButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) UIBarButtonItem *backButton;
@end
