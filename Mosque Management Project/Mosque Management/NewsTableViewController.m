//
//  NewsTableViewController.m
//  Mosque Management
//
//  Created by Developer on 24/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "NewsTableViewController.h"
#import "NewsDetailViewController.h"
#import "NewsCellController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#define NEWS_URL @"http://192.168.1.152:3008/mosque/veloziti/news"
#define IP_ADDRESS @"http://192.168.1.152:3008/"

@interface NewsTableViewController (){
    AFHTTPRequestOperationManager *manager; //to request JSON data
    NSDictionary *newsInfo;                 //to store JSON data
    NSArray *newsData;
    NSString *path;
}

@end

//static NSString *newsUrl = @"http://192.168.1.35:3008/mosque/veloziti/news";

@implementation NewsTableViewController


#pragma mark - Life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
	[HUD show: YES];
    
	[self fetchNewsList];
    HUD.labelText = @"  Loading  ";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationItem.title = @"News";
    
    //setting more tab navigation properties
    [self.tabBarController.moreNavigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    [self.tabBarController.moreNavigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.tabBarController.moreNavigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    //setting tab bar icon color
    [self.tabBarController.tabBar setTintColor:[UIColor colorWithRed:116/255.0 green:173/255.0 blue:6/255.0 alpha:1]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"main page appear called");
    //block the reload table
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"main page disappear called");
}


#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [newsData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    NewsCellController *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NewsCellController alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.cellHeading.numberOfLines = 3;
    [cell.cellSubHeading sizeToFit];
    
    NSDictionary *tempNewsData = [newsData objectAtIndex:indexPath.row];
    
    cell.cellHeading.text = [tempNewsData valueForKey:@"title"];
    cell.cellSubHeading.text = [tempNewsData valueForKey:@"description"];
    
    NSString *imageUrl = [tempNewsData valueForKey:@"imageUrl"];
    if (![[imageUrl substringToIndex:5] isEqualToString:@"https"]) {
        imageUrl = [NSString stringWithFormat:@"%@%@/%@",IP_ADDRESS,path,[tempNewsData valueForKey:@"imageUrl"]];
    }
    NSLog(@"image url:%@",imageUrl);

    UIImageView *temp = [[UIImageView alloc] init];
    
    [temp setImageWithURL:[NSURL URLWithString:imageUrl]  success:^(UIImage *image, BOOL cached) {
        
        cell.cellImage.image = temp.image;
        
        
        
    } failure:^(NSError *error) {
        
        cell.cellImage.image= [UIImage imageNamed:@"defaultimage.png"];
        
    }];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NewsDetailViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"detailView"];
    /*NSDictionary *tempNewsData = [newsData objectAtIndex:indexPath.row];
    obj.pageData = tempNewsData;*/
    [self.navigationController pushViewController:obj animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0;
}

#pragma mark - private methods

//function to reload table
-(void)refreshView:(UIRefreshControl *)refresh
{
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    [self fetchNewsList];
    [self.tableView reloadData];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@",[formatter stringFromDate:[NSDate date]]];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}

- (void)fetchNewsList{
    newsInfo = nil;
    manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSString *mosqueData = @"{\"mosque_name\":\"Masjid-e-mahal\"}";                                    //string to be passed as parameter
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:mosqueData,@"data",nil]; //storing parameter in dictionary to be passed in url
    
    [manager GET:NEWS_URL parameters:parameters success:^(AFHTTPRequestOperation *operation,id data)
     {
         newsInfo = data;
         //NSLog(@"main data:%@",newsInfo);
         newsData = [newsInfo objectForKey:@"data"];
         //NSLog(@"news data:%@",newsData);
         path = [newsInfo objectForKey:@"path"];
         //NSLog(@"path data:%@",path);
         
         [self.tableView reloadData];
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [HUD hide: YES];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         HUD.labelText = @"  Failed  ";
         
         [HUD hide: YES];
         
     }];
    
}

@end
