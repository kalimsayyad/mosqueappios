//
//  MessagesTableViewController.m
//  Mosque Management
//
//  Created by Developer on 27/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "MessagesTableViewController.h"
#import "MessageDetailsViewController.h"
#import "MessagesCellController.h"

@interface MessagesTableViewController ()

@end

@implementation MessagesTableViewController

@synthesize messageType,podcastHeadings,podcastSubHeadings,podcastImages;
@synthesize videoHeadings,videoSubHeadings,videoImages;

#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    podcastHeadings = [NSArray arrayWithObjects:
                       @"Bayan on Friday",@"Eid-ul-adha Bayan",@"Eid-ul-fitra Bayan",@"Bayan on Zakat",
                       @"Bayan on Shab-e-Barat",@"Bayan on Friday",@"Eid-ul-adha Bayan",@"Eid-ul-fitra Bayan",@"Bayan on Zakat",
                       @"Bayan on Shab-e-Barat",@"Bayan on Friday",@"Eid-ul-adha Bayan",nil];
    
    podcastSubHeadings = [NSArray arrayWithObjects:
                          @"Given by Maulana Shaikh Abdulla",@"Mufti Khalid Qusmi say's",@"Given by Maulana Muzzafar to people",@"Mufti abdullha said in Masjid",
                          @"Given by Sayed Ifteqa",@"Given by Maulana Shaikh Abdulla",@"Mufti Khalid Qusmi say's",@"Given by Maulana Muzzafar to people",@"Mufti abdullha said in Masjid",
                          @"Given by Sayed Ifteqa",@"Given by Maulana Shaikh Abdulla",@"Mufti Khalid Qusmi say's",nil];
    
    podcastImages = [NSArray arrayWithObjects:
                     @"image1.png",@"image2.png",@"image3.png",@"image4.png",
                     @"image5.png",@"image6.png",@"image7.png",@"image8.png",
                     @"image9.png",@"image10.png",@"image11.png",@"image12.png",nil];
    
    videoHeadings = [NSArray arrayWithObjects:
                     @"Bayan on Zakat",@"Eid-ul-fitra Bayan",@"Bayan on Shab-e-Barat",@"Eid-ul-adha Bayan",@"Bayan on Friday",@"Bayan on Zakat",@"Eid-ul-fitra Bayan",@"Bayan on Shab-e-Barat",@"Eid-ul-adha Bayan",@"Bayan on Friday",@"Bayan on Zakat",@"Eid-ul-fitra Bayan",nil];
    
    videoSubHeadings = [NSArray arrayWithObjects:
                        @"Given by Sayed Ifteqa",@"Mufti abdullha said in Masjid",@"Given by Sayed Ifteqa",@"Given by Maulana Muzzafar to people",@"Given by Maulana Shaikh Abdulla",@"Given by Sayed Ifteqa",@"Mufti abdullha said in Masjid",@"Given by Sayed Ifteqa",@"Given by Maulana Muzzafar to people",@"Given by Maulana Shaikh Abdulla",@"Given by Sayed Ifteqa",@"Mufti abdullha said in Masjid",nil];
    
    videoImages = [NSArray arrayWithObjects:
                   @"image1.png",@"image2.png",@"image3.png",@"image4.png",
                   @"image5.png",@"image6.png",@"image7.png",@"image8.png",
                   @"image9.png",@"image10.png",@"image11.png",@"image12.png",nil];
    
    /*UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    self.navigationController.navigationBar.topItem.leftBarButtonItem = logoutButton;*/
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar320x44.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"Messages";
    
    selectedHeading = podcastHeadings;
    selectedSubHeading = podcastSubHeadings;
    selectedImages = podcastImages;
    selectedTitle = @"Podcast";
    type = @"audio";
    
    //[messageType setFrame:CGRectMake(60.0, 0, 200.0, 30.0)];
	// Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubview{
    [messageType setFrame:CGRectMake(60.0, 0, 200.0, 30.0)];
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"message page appear called");
    //block the reload table
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"message page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mainSegmentControl:(UISegmentedControl *)segment
{
    
    switch (segment.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"Segment 1 selected.");
            selectedHeading = podcastHeadings;
            selectedSubHeading = podcastSubHeadings;
            selectedImages = podcastImages;
            selectedTitle = @"Podcast";
            type = @"audio";
            [self.tableView reloadData];
            
            break;
        case 1:
            NSLog(@"Segment 2 selected.");
            selectedHeading = videoHeadings;
            selectedSubHeading = videoSubHeadings;
            selectedImages = videoImages;
            selectedTitle = @"Videos";
            type = @"video";
            [self.tableView reloadData];
            break;
    }
}

- (IBAction) segmentedControlIndexChanged
{
    switch (messageType.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"Segment 1 selected.");
            selectedHeading = podcastHeadings;
            selectedSubHeading = podcastSubHeadings;
            selectedImages = podcastImages;
            selectedTitle = @"Podcast";
            type = @"audio";
            [self.tableView reloadData];
            
            break;
        case 1:
            NSLog(@"Segment 2 selected.");
            selectedHeading = videoHeadings;
            selectedSubHeading = videoSubHeadings;
            selectedImages = videoImages;
            selectedTitle = @"Videos";
            type = @"video";
            [self.tableView reloadData];
            break;
    }
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"selected array count: %d",[selectedHeading count]);
    return [selectedHeading count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    MessagesCellController *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MessagesCellController alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.headingLabel.text = [selectedHeading objectAtIndex:indexPath.row];
    cell.subHeadingLabel.text = [selectedSubHeading objectAtIndex:indexPath.row];
    if ([type isEqualToString:@"audio"]) {
        cell.cellImage.image = [UIImage imageNamed:@"podcaster.png"];
    }
    else{
        cell.cellImage.image = [UIImage imageNamed:@"videocaster.png"];
    }
    //cell.imageView.image = [UIImage imageNamed:@"music-note.png"];
    //[UIImage imageNamed:[selectedImages objectAtIndex:indexPath.row]];
    
    
   // [cell setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:0.5]];
    
    
    /*CGFloat widthScale = 50 / cell.imageView.image.size.width;
    CGFloat heightScale = 50 / cell.imageView.image.size.height;
    NSLog(@"width:%f height:%f",cell.imageView.image.size.width,cell.imageView.image.size.height);
    cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);*/
    /*cell.cellImage.layer.cornerRadius = 10.0;
    cell.cellImage.layer.masksToBounds = YES;
    [cell.cellImage.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [cell.cellImage.layer setBorderWidth:1];*/
    
    return cell;
}



/*- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

 UILabel *myLabel = [[UILabel alloc] init]; //creating label
 myLabel.frame = CGRectMake(20, 8, 320, 20); //asigning dimensions
 myLabel.font = [UIFont boldSystemFontOfSize:18];
 myLabel.textColor = [UIColor grayColor];
 
 //titleForHeaderInSection:section returns title text
 myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
 
 UIView *headerView = [[UIView alloc] init]; //creating view
 headerView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
 [headerView addSubview:myLabel]; //adding myLabel to headerView
 
 return headerView;

     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 44)]; // x,y,width,height
     if(section == 0) {
         
         
         NSArray *itemArray = [NSArray arrayWithObjects: @"Podcast", @"Videos", nil];
         UISegmentedControl *control = [[UISegmentedControl alloc] initWithItems:itemArray];
         [control setFrame:CGRectMake(60.0, 0, 200.0, 30.0)];
         //[control setSelectedSegmentIndex:0];
         control.selectedSegmentIndex = 0;
         [control setEnabled:YES];
         [control addTarget:self action:@selector(mainSegmentControl:) forControlEvents:UIControlEventValueChanged];
         [headerView addSubview:control];
         
     }
     return headerView;
 }
 
 - (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
 {
 CGFloat result = 40;
 return result;
 }
 
 
 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 
 NSString *heading; //heading to be returned
 
 heading =  selectedTitle;
 return heading;
 }*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"selected");
    MessageDetailsViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"messageDetailView"];
    //obj.type = type;
    [self.navigationController pushViewController:obj animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}

#pragma mark - private methods

/*- (void) logout
 {
 NSLog(@"logout called");
 [self.tabBarController.navigationController popViewControllerAnimated:YES];
 
 //PUSH : [self.tabBarController.navigationController pushViewController:objNav animated:YES];
 
 //POP  : [self.tabBarController.navigationController popViewControllerAnimated:YES];
 
 }*/

//function to reload table
-(void)refreshView:(UIRefreshControl *)refresh
{
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    
    [self.tableView reloadData];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@",[formatter stringFromDate:[NSDate date]]];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
}

@end
