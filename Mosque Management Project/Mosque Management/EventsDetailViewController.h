//
//  EventsDetailViewController.h
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>

@interface EventsDetailViewController : UIViewController <UIActionSheetDelegate, MKMapViewDelegate, UIScrollViewDelegate,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

/*UIActionSheetDelegate: to implement action sheet
 MFMailComposeViewControllerDelegate: to implement email functionality
 MFMessageComposeViewControllerDelegate: to implement message functionality
 UIWebViewDelegate: to use webview delegate methods
 */

@property (weak, nonatomic) NSDictionary *eventData;        //to store event data
@property (strong, nonatomic) UIBarButtonItem *shareButton; //for share button
@property (weak, nonatomic) NSString *path;

@end
