//
//  MainViewController.h
//  Mosque Management
//
//  Created by Developer on 20/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) NSArray *headings;
@property (strong,nonatomic) NSArray *subHeadings;
@property (strong,nonatomic) NSArray *images;
@property (weak, nonatomic) IBOutlet UITableView *dataTable;
@end
