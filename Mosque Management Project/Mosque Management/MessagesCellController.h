//
//  MessagesCellController.h
//  Mosque Management
//
//  Created by Developer on 28/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

//custom class for cells in messages list
@interface MessagesCellController : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;   //to display image in cell
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;    //to display heading in cell
@property (weak, nonatomic) IBOutlet UILabel *subHeadingLabel; //to display subheading in cell

@end
