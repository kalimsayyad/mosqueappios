//
//  EventsViewController.h
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *upcomingEvents,*pastEvents;
@property (strong,nonatomic) NSArray *subHeadings1,*subHeading2;
@property (strong,nonatomic) NSArray *pastDates,*upComeDates;
@property (strong,nonatomic) NSArray *pastMonths,*upComeMonths;

@end
