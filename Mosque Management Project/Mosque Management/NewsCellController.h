//
//  NewsCellController.h
//  Mosque Management
//
//  Created by Developer on 30/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

//custom class for cells in news list
@interface NewsCellController : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;  //to display image in cell
@property (weak, nonatomic) IBOutlet UILabel *cellHeading;    //to display heading in cell
@property (weak, nonatomic) IBOutlet UILabel *cellSubHeading; //to display subheading in cell

@end
