//
//  VideoViewController.m
//  Mosque Management
//
//  Created by Developer on 21/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "VideoViewController.h"

@interface VideoViewController ()

@end

@implementation VideoViewController
@synthesize webView;

#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
    NSURL *url = [NSURL URLWithString:@"https://www.youtube.com/watch?v=tbRMWIrfBiY"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
     */
    
    //https://www.youtube.com/v/jDNMOHAKBI
    NSString *htmlString = @"<html><head><meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = 212\"/></head><body style=\"margin-top:0px;margin-left:0px\"><div><object width=\"320\" height=\"480\"><param name=\"movie\" value=\"http://www.youtube.com/v/oHg5SJYRHA0&f=gdata_videos&c=ytapi-my-clientID&d=nGF83uyVrg8eD4rfEkk22mDOl3qUImVMV6ramM\"></param><param name=\"wmode\" value=\"transparent\"></param><embed src=\"http://www.youtube.com/v/YteU5U42vi0\"type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"320\" height=\"480\"></embed></object></div></body></html>";
 
    [webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"https://www.youtube.com/v/YteU5U42vi0"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


@end
