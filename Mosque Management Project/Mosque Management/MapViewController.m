//
//  MapViewController.m
//  Mosque Management
//
//  Created by Syed Adnan on 07/02/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface MapViewController ()
{
    GMSMapView *mapView_;
    GMSCameraPosition *camera ;
    //CLLocationManager *locationManager;
    
    float latitude,longitude;
}


@end

@implementation MapViewController
@synthesize  backButton,locationManager,mosqueLatitude,mosqueLongitude,mosqueName,mosqueAddress;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    /*mosqueLatitude = 24.412999;
    mosqueLongitude= 54.474489;*/
    
    camera = [GMSCameraPosition cameraWithLatitude:mosqueLatitude
                                         longitude:mosqueLongitude
                                              zoom:14];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.settings.compassButton=YES;
    mapView_.settings.myLocationButton=YES;
    self.view=mapView_;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"Map";

    backButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
     self.navigationItem.leftBarButtonItem = backButton;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(mosqueLatitude , mosqueLongitude);
    marker.title = mosqueName;//@"Shaikh Zayad Grant Mosque";
    marker.snippet = mosqueAddress;//@"Abu Dhabi";
    marker.map = mapView_;
    locationManager = [[CLLocationManager alloc] init];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - location manager methods
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        latitude=currentLocation.coordinate.latitude;
        longitude=currentLocation.coordinate.longitude;
    }
}

#pragma mark - private methods
-(void) closeView{
    NSLog(@"close view called");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
