//
//  MainViewController.m
//  Mosque Management
//
//  Created by Developer on 20/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "MainViewController.h"
#import "NewsDetailViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize headings,subHeadings,images,dataTable;


#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    headings = [NSArray arrayWithObjects:
                @"Title1",@"Title2",@"Title3",@"Title4",
                @"Title5",@"Title6",@"Title7",@"Title8",
                @"Title9",@"Title10",@"Title11",@"Title12",nil];
    
    subHeadings = [NSArray arrayWithObjects:
                   @"Detail about cell 1",@"Detail about cell 2",@"Detail about cell 3",@"Detail about cell 4",
                   @"Detail about cell 5",@"Detail about cell 6",@"Detail about cell 7",@"Detail about cell 8",
                   @"Detail about cell 9",@"Detail about cell 10",@"Detail about cell 11",@"Detail about cell 12",nil];
    
    images = [NSArray arrayWithObjects:
              @"image1.png",@"image2.png",@"image3.png",@"image4.png",
              @"image5.png",@"image6.png",@"image7.png",@"image8.png",
              @"image9.png",@"image10.png",@"image11.png",@"image12.png",nil];
    
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    self.navigationController.navigationBar.topItem.leftBarButtonItem = logoutButton;
    self.navigationController.navigationBar.topItem.title = @"News";
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"main page appear called");
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"main page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [headings count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [headings objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [subHeadings objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    
    CGFloat widthScale = 50 / cell.imageView.image.size.width;
    CGFloat heightScale = 50 / cell.imageView.image.size.height;
    cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
    cell.imageView.layer.cornerRadius = 20.0;
    [cell.imageView.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [cell.imageView.layer setBorderWidth:7];
    
    return cell;
}

/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init]; //creating label
    myLabel.frame = CGRectMake(20, 8, 320, 20); //asigning dimensions
    myLabel.font = [UIFont boldSystemFontOfSize:18];
    myLabel.textColor = [UIColor grayColor];
    
    //titleForHeaderInSection:section returns title text
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] init]; //creating view
    headerView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    [headerView addSubview:myLabel]; //adding myLabel to headerView
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat result = 40;
    return result;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *heading; //heading to be returned
    
    heading =  @"News";
    return heading;
}*/


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"selected");
    NewsDetailViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"detailView"];
    [self.navigationController pushViewController:obj animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}

#pragma mark - private methods

- (void) logout
{
    NSLog(@"logout called");
    [self.tabBarController.navigationController popViewControllerAnimated:YES];
    
    /*PUSH : [self.tabBarController.navigationController pushViewController:objNav animated:YES];
    
    POP  : [self.tabBarController.navigationController popViewControllerAnimated:YES];*/

}

@end
