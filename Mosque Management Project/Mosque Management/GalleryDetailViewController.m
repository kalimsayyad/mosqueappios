//
//  GalleryDetailViewController.m
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "GalleryDetailViewController.h"
#import <Social/Social.h>
@interface GalleryDetailViewController ()

@end

@implementation GalleryDetailViewController
@synthesize imageName,imageContainer,backButton,shareButton,imageUrl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    imageContainer.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
    backButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
    
    shareButton = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(displayActionSheet:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.rightBarButtonItem = shareButton;
    self.navigationItem.title = imageName;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"gallery detail page appear called");
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"gallery detail page disappear called");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
-(void) closeView{
    NSLog(@"close view called");
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - action sheet methods
- (IBAction)displayActionSheet:(id)sender
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Mail", @"Message", @"Facebook", @"Twitter", nil];
        
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        [actionSheet showFromBarButtonItem:shareButton animated:YES];
    }
    
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
    {
        switch (buttonIndex)
        {
            case 0:
            {
                NSLog(@"email button clicked.");
                // Email Subject
                NSString *emailTitle = @"Test Email";
                // Email Content
                NSString *messageBody = @"iOS programming is so fun!";
                // To address
                NSArray *toRecipents = [NSArray arrayWithObject:@"anand.bhunia@veloziti.com"];
                
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                mc.mailComposeDelegate = self;
                [mc setSubject:emailTitle];
                [mc setMessageBody:messageBody isHTML:NO];
                [mc setToRecipients:toRecipents];
                // Present mail view controller on screen
                [self presentViewController:mc animated:YES completion:NULL];
            }
            break;
            
            case 1:
            {
                NSLog(@"sms button clicked.");
                [self showSMS:(NSString *)imageUrl];
            }
                break;
                
            case 2:
                
            {
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
                {
                    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                    [tweetSheet setInitialText:[NSString stringWithFormat:@"Check out this image\n link : %@",imageUrl]];
                    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
                    {
                        switch(result)
                        {
                                //  This means the user cancelled without sending the Tweet
                            case SLComposeViewControllerResultCancelled:
                            {
                                UIAlertView *alertViewCancel = [[UIAlertView alloc]
                                                                initWithTitle:@"Sorry"
                                                                message:@"Status is not Updated"
                                                                delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                                [alertViewCancel show];
                                break;
                            }
                                //  This means the user hit 'Send'
                            case SLComposeViewControllerResultDone:
                            {
                                UIAlertView *alertViewDone = [[UIAlertView alloc]
                                                              initWithTitle:@"Success"
                                                              message:@"Status is Update"
                                                              delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                                [alertViewDone show];
                                break;
                            }
                                break;
                        }
                    };
                    
                    
                    
                    [self presentViewController:tweetSheet animated:YES completion:nil];
                    
                }
                else
                {
                    UIAlertView *alertView = [[UIAlertView alloc]
                                              initWithTitle:@"Sorry"
                                              message:@"You can't update status right now, make sure your device has an internet connection and you have at least one facebook account setup"
                                              delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }
                
                
                
                break;
                
            case 3:
            {
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
                {
                    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                    [tweetSheet setInitialText:[NSString stringWithFormat:@"Check out this image\n link : %@",imageUrl]];
                    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
                    {
                        switch(result)
                        {
                                //  This means the user cancelled without sending the Tweet
                            case SLComposeViewControllerResultCancelled:
                            {
                                UIAlertView *alertViewCancel = [[UIAlertView alloc]
                                                                initWithTitle:@"Sorry"
                                                                message:@"Twitt is not post"
                                                                delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                                [alertViewCancel show];
                                break;
                            }
                                //  This means the user hit 'Send'
                            case SLComposeViewControllerResultDone:
                            {
                                UIAlertView *alertViewDone = [[UIAlertView alloc]
                                                              initWithTitle:@"Success"
                                                              message:@"Twitt is post"
                                                              delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                                [alertViewDone show];
                                break;
                            }
                                break;
                        }
                    };
                    
                    
                    
                    [self presentViewController:tweetSheet animated:YES completion:nil];
                    
                }
                else
                {
                    UIAlertView *alertView = [[UIAlertView alloc]
                                              initWithTitle:@"Sorry"
                                              message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                              delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }

                break;
        }
    }
    
    
#pragma mark - mail composition methods
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
    {
        switch (result)
        {
            case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
            case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
            case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
            case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
            default:
            break;
        }
        
        // Close the Mail Interface
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
#pragma mark - SMS composition methods
- (void)showSMS:(NSString*)messageText {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[@"12345678", @"72345524"];
    NSString *message = [NSString stringWithFormat:@"Just sent the %@ file to your email. Please check!", messageText];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:NULL];
}
    
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
    {
        switch (result) {
            case MessageComposeResultCancelled:
            break;
            
            case MessageComposeResultFailed:
            {
                UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [warningAlert show];
                break;
            }
            
            case MessageComposeResultSent:
            break;
            
            default:
            break;
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }

@end
