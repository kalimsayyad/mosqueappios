//
//  MessageViewController.h
//  Mosque Management
//
//  Created by Developer on 21/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface MessageViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    NSString *type;     //to store message type(podcast/video)
    MBProgressHUD *HUD; //to show the loading progress
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *messageType; //to select message type
- (IBAction) segmentedControlIndexChanged;                            //function to handle message type selection
@property (weak, nonatomic) IBOutlet UITableView *messageTable;       //to display message list

@end
