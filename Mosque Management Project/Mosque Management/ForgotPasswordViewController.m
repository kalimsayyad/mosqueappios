//
//  ForgotPasswordViewController.m
//  Mosque Management
//
//  Created by Developer on 17/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "LoginViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController
@synthesize thirdAnsTextField,submitBtn,backButton;

#pragma mark - life cycle functions

- (void)viewDidLoad
{
    [super viewDidLoad];
    thirdAnsTextField.delegate = self;
    
    submitBtn.clipsToBounds = YES;
    submitBtn.layer.cornerRadius = 4;
    [submitBtn setBackgroundImage:[UIImage imageNamed:@"button_green.png"] forState:UIControlStateNormal];
    //[submitBtn setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:200.0f/255.0f blue:223.0f/255.0f alpha:1.0f]];
    //[submitBtn.layer setBorderColor: [[UIColor colorWithRed:0.0f/255.0f green:137.0f/255.0f blue:223.0f/255.0f alpha:1.0f] CGColor]];
    //[submitBtn.layer setBorderWidth: 1];
    
    backButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeView)];
    [backButton setTintColor:[UIColor whiteColor]];
    
    self.navigationItem.leftBarButtonItem = backButton;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

    self.navigationItem.title = @"Forgot Password";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    
    /*UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background_2.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];*/
	// Do any additional setup after loading the view.
    UIColor *color = [UIColor darkGrayColor];
    thirdAnsTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your email address" attributes:@{NSForegroundColorAttributeName: color}];
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"forgot page appear called");
    [self.navigationController setNavigationBarHidden:NO];    // it shows
    //self.navigationController.navigationBar.topItem.title = @"Forgot Password";
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"forgot page disappear called");
    [self.navigationController setNavigationBarHidden:YES];    // it shows
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button functions
- (IBAction)submitButton:(id)sender {
}

#pragma mark - textFieldDelegate functions
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //to reset position of the view
    CGRect rect = self.view.frame;
    CGFloat value = self.view.frame.origin.y;
    if  (self.view.frame.origin.y < 0)
    {
        rect.origin.y -= value;
        rect.size.height += value;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
    return YES;
}

-(void)touchesBegan:(NSSet *) touches withEvent: (UIEvent *) event
{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    
    //to reset position of the view
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect rect = self.view.frame;
    CGFloat value = self.view.frame.origin.y;
    if  (self.view.frame.origin.y < 0)
    {
        rect.origin.y -= value;
        rect.size.height += value;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect rect = self.view.frame;
    
    if ([textField isEqual:thirdAnsTextField])
    {
        if (self.view.frame.origin.y >= 0)
        {
            rect.origin.y -= 120;
            rect.size.height += 120;
        }
        else if(self.view.frame.origin.y == -80)
        {
            rect.origin.y -= 40;
            rect.size.height += 40;
        }
        self.view.frame = rect;
    }
    [UIView commitAnimations];
    NSLog(@"(%f)",self.view.frame.origin.y);
}

#pragma mark - private methods
-(void) closeView{
    NSLog(@"close view called");
    LoginViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    [self.navigationController pushViewController:obj animated:YES];
    [UIView commitAnimations];
}

@end
