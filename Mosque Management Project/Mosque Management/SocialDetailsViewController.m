//
//  SocialDetailsViewController.m
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "SocialDetailsViewController.h"

@interface SocialDetailsViewController ()

@end

@implementation SocialDetailsViewController
@synthesize webView,website;

#pragma mark - Life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    webView.delegate = self;
    
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center=self.view.center;
    [self.view addSubview:activityView];

    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    
    NSURL *url;
    
    if([website isEqualToString:@"Facebook"])
    {
        url = [NSURL URLWithString:@"https://www.facebook.com/Sheikh.Zayed.Grand.Mosque"];
    }
    else if ([website isEqualToString:@"Twitter"])
    {
        url = [NSURL URLWithString:@"https://twitter.com/SheikhZayed"];
    }
    
    self.navigationItem.title = website;

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - webView load methods
- (void) webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"webview loading started");
    
    [activityView startAnimating];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"webview loaded");
    [activityView stopAnimating];
}

@end
