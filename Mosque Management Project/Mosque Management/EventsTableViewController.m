//
//  EventsTableViewController.m
//  Mosque Management
//
//  Created by Developer on 27/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "EventsTableViewController.h"
#import "EventsDetailViewController.h"
#import "EventCellController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#define EVENTS_URL @"http://192.168.1.152:3008/mosque/veloziti/events"
#define IP_ADDRESS @"http://192.168.1.152:3008/"

@interface EventsTableViewController (){
    AFHTTPRequestOperationManager *manager; //to request JSON data
    NSDictionary *eventsInfo;                 //to store JSON data
    NSArray *eventsData;
    NSString *path;
    NSMutableArray *upcomingEvents;         //to store upcoming events
    NSMutableArray *pastEvents;             //to store past events
}


@end

//static NSString *eventsUrl = @"http://192.168.1.35:3008/mosque/veloziti/events";

@implementation EventsTableViewController
    
#pragma mark - Life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Initializing the HUD and adding it to the window.
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
	HUD = [[MBProgressHUD alloc] initWithWindow:window];
	
	[window addSubview:HUD];
	[HUD show: YES];
	
    [self fetchEventsList];
    
    HUD.labelText = @"  Loading  ";
    
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"Events";
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"events page appear called");
    
    //block the reload table
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"events page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int count;
    if (section == 0) {
        count = [upcomingEvents count];
    }
    else
    {
        count = [pastEvents count];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"customCell";

    EventCellController *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[EventCellController alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.headingLabel.numberOfLines = 2;
    [cell.subheadingLabel sizeToFit];
    
    if (indexPath.section == 0) {
        NSDictionary *temp = [upcomingEvents objectAtIndex:indexPath.row];
        cell.headingLabel.text = [temp valueForKey:@"title"];
        cell.subheadingLabel.text = [temp valueForKey:@"description"];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"MMM dd,yyyy";
        NSDate *yourDate = [dateFormatter dateFromString:[temp valueForKey:@"eventdate"]];
        dateFormatter.dateFormat = @"MMM";
        cell.month.text = [dateFormatter stringFromDate:yourDate];
        dateFormatter.dateFormat = @"dd";
        cell.date.text = [dateFormatter stringFromDate:yourDate];
    }
    else if (indexPath.section == 1)
    {
        NSDictionary *temp = [pastEvents objectAtIndex:indexPath.row];
        cell.headingLabel.text = [temp valueForKey:@"title"];
        cell.subheadingLabel.text = [temp valueForKey:@"description"];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"MMM dd,yyyy";
        NSDate *yourDate = [dateFormatter dateFromString:[temp valueForKey:@"eventdate"]];
        dateFormatter.dateFormat = @"MMM";
        cell.month.text = [dateFormatter stringFromDate:yourDate];
        dateFormatter.dateFormat = @"dd";
        cell.date.text = [dateFormatter stringFromDate:yourDate];
    }
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init]; //creating label
    myLabel.frame = CGRectMake(0, 3, 320, 20); //asigning dimensions
    myLabel.font = [UIFont boldSystemFontOfSize:14];
    myLabel.textColor = [UIColor blackColor];
    myLabel.textAlignment = NSTextAlignmentCenter;
    
    //titleForHeaderInSection:section returns title text
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] init]; //creating view
    headerView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    [headerView addSubview:myLabel]; //adding myLabel to headerView
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat result = 26;
    return result;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *heading; //heading to be returned
    if (section == 0) {
        heading = @"Upcoming Events";
    }
    else
    {
        heading =  @"Past Events";
    }
    return heading;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"selected");
    EventsDetailViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"eventDetailView"];
    if (indexPath.section == 0) {
        NSDictionary *temp = [upcomingEvents objectAtIndex:indexPath.row];
        obj.eventData = temp;
        obj.path = path;
    }
    else
    {
        NSDictionary *temp = [pastEvents objectAtIndex:indexPath.row];
        obj.eventData = temp;
        obj.path = path;
    }
    
    [self.navigationController pushViewController:obj animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 62.0;
}

#pragma mark - private methods

//function to reload table
-(void)refreshView:(UIRefreshControl *)refresh
{
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    
    [self fetchEventsList];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@",[formatter stringFromDate:[NSDate date]]];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    [refresh endRefreshing];
    [self.tableView reloadData];

}

-(void)fetchEventsList
{
    upcomingEvents = [[NSMutableArray alloc] init];
    pastEvents = [[NSMutableArray alloc] init];
    
    manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSString *mosqueData = @"{\"mosque_name\":\"Fatima Meher\"}";                                   //string to be passed as parameter
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:mosqueData,@"data",nil]; //storing parameter in dictionary to be passed in url
    
    [manager GET:EVENTS_URL parameters:parameters success:^(AFHTTPRequestOperation *operation,id data)
     {
         eventsInfo = data;
         //NSLog(@"main data:%@",eventsInfo);
         eventsData = [eventsInfo objectForKey:@"data"];
         NSLog(@"events data:%@",eventsData);
         path = [eventsInfo objectForKey:@"path"];
         //NSLog(@"path data:%@",path);
         
         NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
         for (id element in eventsData)
         {
             NSDictionary *myData = element;
             
             //dateFormatter.dateFormat = @"dd-MM-yyyy";
             dateFormatter.dateFormat = @"MMM dd,yyyy";
             NSDate *yourDate = [dateFormatter dateFromString:[myData valueForKey:@"eventdate"]];
             dateFormatter.dateFormat = @"dd-MM-yyyy";
             NSLog(@"your date:%@",[dateFormatter stringFromDate:yourDate]);
             
             NSDate *currentDate = [NSDate date];
             NSLog(@"current date:%@",[dateFormatter stringFromDate:currentDate]);
             
             unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
             NSCalendar* calendar = [NSCalendar currentCalendar];
             NSDateComponents* yourDateComponents = [calendar components:flags fromDate:yourDate];
             NSDateComponents* currentDateComponents = [calendar components:flags fromDate:currentDate];
             yourDate = [calendar dateFromComponents:yourDateComponents];
             currentDate = [calendar dateFromComponents:currentDateComponents];
             
             if(([yourDate compare:currentDate] == NSOrderedDescending) || ([yourDate compare:currentDate] == NSOrderedSame))
             {
                 NSLog(@"upcoming");
                 [upcomingEvents addObject:element];
             }
             else if ([yourDate compare:currentDate] == NSOrderedAscending)
             {
                 NSLog(@"past");
                 [pastEvents addObject:element];
             }
         }
         
         NSLog(@"Upcoming events:%@",upcomingEvents);
         //NSLog(@"Past events:%@",pastEvents);
         
         //for sorting the list according to date
         dateFormatter.dateFormat = @"MMM dd,yyyy";
         NSComparator compareDates = ^(id string1, id string2)
         {
             NSDate *date1 = [dateFormatter dateFromString:string1];
             NSDate *date2 = [dateFormatter dateFromString:string2];
             
             return [date1 compare:date2];
         };
         
         NSSortDescriptor *upcomingDescriptor = [[NSSortDescriptor alloc] initWithKey:@"eventdate" ascending:YES comparator:compareDates];
         [upcomingEvents sortUsingDescriptors:[NSArray arrayWithObject:upcomingDescriptor]];
         
         NSSortDescriptor *pastDescriptor = [[NSSortDescriptor alloc] initWithKey:@"eventdate" ascending:NO comparator:compareDates];
         [pastEvents sortUsingDescriptors:[NSArray arrayWithObject:pastDescriptor]];
         NSLog(@"Upcoming events after sort:%@",upcomingEvents);
         
         [self.tableView reloadData];
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [HUD hide: YES];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         HUD.labelText = @"  Failed  ";
         
         [HUD hide: YES];
         
     }];
    
}

@end
