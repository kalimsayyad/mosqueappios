//
//  LoginViewController.m
//  Mosque Management
//
//  Created by Developer on 17/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "RegisterViewController.h"
#import "NewsTableViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize usernameTextField,passwordTextField,loginBtn,registerBtn;

#pragma mark - life cycle functions

- (void)viewDidLoad
{
    [super viewDidLoad];
    usernameTextField.delegate = self;
    passwordTextField.delegate = self;
    
    loginBtn.clipsToBounds = YES;
    loginBtn.layer.cornerRadius = 4;
    //[loginBtn setBackgroundImage:[UIImage imageNamed:@"button_blue.png"] forState:UIControlStateNormal];
    
    registerBtn.clipsToBounds = YES;
    registerBtn.layer.cornerRadius = 4;
    //[registerBtn setBackgroundImage:[UIImage imageNamed:@"button_blue.png"] forState:UIControlStateNormal];
    //[loginBtn.layer setBorderColor: [[UIColor colorWithRed:0.0f/255.0f green:137.0f/255.0f blue:223.0f/255.0f alpha:1.0f] CGColor]];
    //[loginBtn.layer setBorderWidth: 1];
    
    self.navigationItem.title = @"Login";
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background_mix.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    UIColor *color = [UIColor darkGrayColor];
    usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"User name" attributes:@{NSForegroundColorAttributeName: color}];
    passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"login page appear called");
    [self.navigationController setNavigationBarHidden:YES];    // it shows
    //self.navigationController.navigationBar.topItem.title = @"Login";
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"login page disappear called");
    [self.navigationController setNavigationBarHidden:YES];    // it shows
}


#pragma mark - button functions

- (IBAction)loginButton:(id)sender
{
    NewsTableViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"homeView"];
    
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    
    [self.navigationController pushViewController:obj animated:YES];
    
    
   [UIView commitAnimations];
}

- (IBAction)forgotPasswordButton:(id)sender
{
    
}

- (IBAction)registerButton:(id)sender
{
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"forgetPasswordSegue"])
    {
        
         ForgotPasswordViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"forgotPassword"];
        
        [UIView beginAnimations:@"View Flip" context:nil];
        [UIView setAnimationDuration:0.80];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
        
        [UIView commitAnimations];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else if([segue.identifier isEqualToString:@"registerSegue"])
    {
        RegisterViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"registration"];
        
        [UIView beginAnimations:@"View Flip" context:nil];
        [UIView setAnimationDuration:0.80];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
        
        [UIView commitAnimations];
        [self.navigationController pushViewController:obj animated:YES];
    }
 }

#pragma mark - textFieldDelegate functions

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet *) touches withEvent: (UIEvent *) event
{    
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

@end
