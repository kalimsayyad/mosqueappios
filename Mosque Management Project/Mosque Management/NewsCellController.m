//
//  NewsCellController.m
//  Mosque Management
//
//  Created by Developer on 30/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "NewsCellController.h"

@implementation NewsCellController

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
