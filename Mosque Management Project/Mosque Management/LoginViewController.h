//
//  LoginViewController.h
//  Mosque Management
//
//  Created by Developer on 17/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate,UIAlertViewDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField; //to accept username
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField; //to accept password

- (IBAction)loginButton:(id)sender;
- (IBAction)forgotPasswordButton:(id)sender;
- (IBAction)registerButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

@end
