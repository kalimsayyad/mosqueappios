//
//  AudioMessageViewController.h
//  Mosque Management
//
//  Created by Developer on 05/02/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioMessageViewController : UIViewController <AVAudioPlayerDelegate,UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

    @property (weak, nonatomic) NSDictionary *currentData;
    
    @property (strong, nonatomic) UIBarButtonItem *shareButton;      //for share button
    @property (weak, nonatomic) IBOutlet UITextView *messageDetails; //to display message details
    @property (weak, nonatomic) IBOutlet UILabel *authorLabel;       //to display message author name
    @property (weak, nonatomic) IBOutlet UILabel *dateLabel;         //to display message date
    @property (weak, nonatomic) IBOutlet UIImageView *imageView;     //to display message image
    @property (weak, nonatomic) IBOutlet UIWebView *audioPlayerView; //to load audio player webpage
    
    //for audio player
    /*@property (weak, nonatomic) IBOutlet UIButton *togglePlayPause;
    - (IBAction)togglePlayPauseTapped:(id)sender;
    
    @property (weak, nonatomic) IBOutlet UILabel *songName;
    @property (weak, nonatomic) IBOutlet UISlider *sliderOutlet;

    @property (strong, nonatomic) AVAudioPlayer *player;
    @property (nonatomic, strong) NSTimer* timer;*/
@end
