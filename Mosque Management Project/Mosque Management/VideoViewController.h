//
//  VideoViewController.h
//  Mosque Management
//
//  Created by Developer on 21/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView; //to load the video webpage

@end
