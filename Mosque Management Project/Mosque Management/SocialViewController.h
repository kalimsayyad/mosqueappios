//
//  SocialViewController.h
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *facebookButton;  //for facebook button
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;   //for twitter button
@property (weak, nonatomic) IBOutlet UIImageView *facebookImage;//for facebook icon
@property (weak, nonatomic) IBOutlet UIImageView *twitterImage; //for twitter icon

@end
