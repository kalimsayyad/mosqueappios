//
//  MessagesTableViewController.h
//  Mosque Management
//
//  Created by Developer on 27/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesTableViewController : UITableViewController

{
    NSArray *selectedHeading;
    NSArray *selectedSubHeading;
    NSArray *selectedImages;
    NSString *selectedTitle;
    NSString *type;
}

@property (strong,nonatomic) NSArray *podcastHeadings;
@property (strong,nonatomic) NSArray *podcastSubHeadings;
@property (strong,nonatomic) NSArray *podcastImages;

@property (strong,nonatomic) NSArray *videoHeadings;
@property (strong,nonatomic) NSArray *videoSubHeadings;
@property (strong,nonatomic) NSArray *videoImages;


@property (weak, nonatomic) IBOutlet UISegmentedControl *messageType;
- (IBAction) segmentedControlIndexChanged;

@end
