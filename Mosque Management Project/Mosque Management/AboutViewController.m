//
//  AboutViewController.m
//  Mosque Management
//
//  Created by Developer on 23/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "AboutViewController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"


@interface AboutViewController (){
    AFHTTPRequestOperationManager *manager;
    NSArray *myArray;
    NSDictionary *tempDictionary;
}

@end

static NSString *aboutUrl = @"https://dl.dropboxusercontent.com/u/65857105/gallery/Mosque/MosqueAbout.json";

@implementation AboutViewController
@synthesize imageView,imamContactLabel,imamNameLabel,visionLabel,scrollView;
@synthesize descriptionLabel,description,locationLabel,location,contactLabel,contact;


#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    tempDictionary = [[NSDictionary alloc] init];
    
    scrollView.scrollEnabled = YES;
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
	[HUD show: YES];
	
    HUD.labelText = @"  Loading  ";
    [self fetchAboutList];

    //NSLog(@"view did load data: %@",tempDictionary);
    /*UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    self.navigationController.navigationBar.topItem.leftBarButtonItem = logoutButton;*/
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar320x44.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"About";
    /*CGRect frame = visionField.frame;
    frame.size.height = visionField.contentSize.height;
    visionField.frame = frame;*/
}

- (void)fetchAboutList{
    NSLog(@"fetch function called");
    manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    
    [manager GET:aboutUrl parameters:nil success:^(AFHTTPRequestOperation *operation,id data)
     {
         NSDictionary * myNewData = data;
         myArray = [myNewData valueForKey:@"About"];
         NSLog(@"fetch: %d",[myArray count]);
         
         tempDictionary  = [myArray objectAtIndex:0];
         
         [self fillData];
         /*for (id element in tempDictionary) {
             NSDictionary *temp = element;
             NSLog(@"temp %@",temp);
         }*/
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [HUD hide: YES];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         HUD.labelText = @"  Failed  ";
         
         [HUD hide: YES];
         
     }];
    
}

-(void)fillData
{
    
    /*
    NSLog(@"fill data called");
    NSLog(@"fill data: %@",[tempDictionary valueForKey:@"vision and dream"]);
    
    visionLabel.text = [tempDictionary valueForKey:@"vision and dream"];
    visionLabel.numberOfLines = 0;
    visionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    descriptionLabel.text = [tempDictionary valueForKey:@"vision and dream"];
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    //Set frame according to string
    CGSize lsize = [visionLabel.text sizeWithFont:visionLabel.font constrainedToSize:CGSizeMake(309, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
    [visionLabel setFrame:CGRectMake( 5 , 188 , lsize.width , lsize.height)];
    
    CGFloat descY = visionLabel.frame.origin.y + visionLabel.frame.size.height + 5;
    
    [description setFrame:CGRectMake( 5 , descY , 83 , 21)];
    
    CGFloat descLY = description.frame.origin.y + description.frame.size.height + 5;
    
    
    CGSize dsize = [descriptionLabel.text sizeWithFont:descriptionLabel.font constrainedToSize:CGSizeMake(309, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
    [descriptionLabel setFrame:CGRectMake( 5 , descLY , dsize.width , dsize.height)];
    
    
    CGFloat height = descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + 5;
    [scrollView setContentSize:CGSizeMake(320, height)];
     
     */
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"about page appear called");
    
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"about page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
