//
//  SubMenuViewController.h
//  Mosque Management
//
//  Created by Developer on 17/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubMenuViewController : UITableViewController
@property (nonatomic, strong) NSArray *menuItems;
@end
