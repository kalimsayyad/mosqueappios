//
//  SocialViewController.m
//  Mosque Management
//
//  Created by Developer on 22/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "SocialViewController.h"
#import "SocialDetailsViewController.h"

@interface SocialViewController ()

@end

@implementation SocialViewController
@synthesize facebookButton,twitterButton,facebookImage,twitterImage;

#pragma mark - Life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    [facebookButton addTarget:self action:@selector(facebook:) forControlEvents:UIControlEventTouchDown];
    [twitterButton addTarget:self action:@selector(twitter:) forControlEvents:UIControlEventTouchDown];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_green.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"Social Media";
	
    facebookButton.layer.cornerRadius = 4;
    [facebookButton setTintColor:[UIColor whiteColor]];
    facebookButton.clipsToBounds = YES;
    [facebookButton.layer setBorderWidth: 0];
    
    twitterButton.layer.cornerRadius = 4;
    twitterButton.clipsToBounds = YES;
    [twitterButton setTintColor:[UIColor whiteColor]];
    [twitterButton.layer setBorderWidth: 0];
    
    facebookImage.layer.cornerRadius = 4;
    facebookImage.clipsToBounds = YES;
    
    twitterImage.layer.cornerRadius = 4;
    twitterImage.clipsToBounds = YES;
}


-(void)viewWillAppear:(BOOL)animated{
    
}

-(void)viewWillDisappear:(BOOL)animated{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button click methods
- (void)facebook:(UIButton*)sender {
    
    SocialDetailsViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"socialView"];
    controller.website = @"Facebook";
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)twitter:(UIButton*)sender {
    
    SocialDetailsViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"socialView"];
    controller.website = @"Twitter";
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
