//
//  MessageDetailsViewController.m
//  Mosque Management
//
//  Created by Developer on 21/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import "MessageDetailsViewController.h"
#import "VideoViewController.h"
#import <Social/Social.h>

@interface MessageDetailsViewController ()

@end

@implementation MessageDetailsViewController
@synthesize shareButton,messageDetails,authorLabel,dateLabel,currentData;
@synthesize videoButton;

#pragma mark - life cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareButton = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(displayActionSheet:)];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    self.navigationItem.title = @"Message Details";
    
    [videoButton addTarget:self action:@selector(playBtnClicked) forControlEvents:UIControlEventTouchDown];
    [videoButton setBackgroundImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:(NSString *)[currentData valueForKey:@"imageUrl"]]]] forState:UIControlStateNormal];
    
    authorLabel.text = [currentData valueForKey:@"author"];
    dateLabel.text = [currentData valueForKey:@"createddate"];
    messageDetails.text = [currentData valueForKey:@"longdescription"];
    messageDetails.textAlignment = NSTextAlignmentJustified;
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"message detail page appear called");
}

-(void)viewWillDisappear:(BOOL)animated{
    NSLog(@"message detail page disappear called");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
-(void) playBtnClicked{
    NSLog(@"button click called");
    VideoViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"videoView"];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - action sheet methods
- (IBAction)displayActionSheet:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Mail", @"Message", @"Facebook", @"Twitter", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showFromBarButtonItem:shareButton animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            NSLog(@"email button clicked.");
            // Email Subject
            //NSString *emailTitle = [currentData valueForKey:@"title"];
            // Email Content
            //NSString *messageBody = [NSString stringWithFormat:@"%@\n Link:%@",[currentData valueForKey:@"description"],[currentData valueForKey:@"webPage"]];
            // To address
            NSArray *toRecipents = [NSArray arrayWithObject:@""];
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:@"title"];
            [mc setMessageBody:@"description" isHTML:NO];
            [mc setToRecipients:toRecipents];
            
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
            break;
            
        case 1:
            NSLog(@"sms button clicked.");
            //[self showSMS:[NSString stringWithFormat:@"%@\n %@\n Link:%@",[currentData valueForKey:@"title"],[currentData valueForKey:@"description"],[currentData valueForKey:@"webPage"]]];
            [self showSMS:@"message"];
            break;
            
        case 2:
            
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                //[tweetSheet setInitialText:[NSString stringWithFormat:@"%@ \n link : %@",[currentData valueForKey:@"title"],[currentData valueForKey:@"webPage"]]];
                [tweetSheet setInitialText:@"tweet"];
                tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
                {
                    switch(result)
                    {
                            //  This means the user cancelled without sending the Tweet
                        case SLComposeViewControllerResultCancelled:
                        {
                            UIAlertView *alertViewCancel = [[UIAlertView alloc]
                                                            initWithTitle:@"Sorry"
                                                            message:@"Status is not Updated"
                                                            delegate:self
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil];
                            [alertViewCancel show];
                            break;
                        }
                            //  This means the user hit 'Send'
                        case SLComposeViewControllerResultDone:
                        {
                            UIAlertView *alertViewDone = [[UIAlertView alloc]
                                                          initWithTitle:@"Success"
                                                          message:@"Status is Update"
                                                          delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                            [alertViewDone show];
                            break;
                        }
                            break;
                    }
                };
                
                
                
                [self presentViewController:tweetSheet animated:YES completion:nil];
                
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Sorry"
                                          message:@"You can't update status right now, make sure your device has an internet connection and you have at least one facebook account setup"
                                          delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                [alertView show];
            }
        }
            
            
            
            break;
            
        case 3:
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                //[tweetSheet setInitialText:[NSString stringWithFormat:@"%@ \n link : %@",[currentData valueForKey:@"title"],[currentData valueForKey:@"webPage"]]];
                [tweetSheet setInitialText:@"status"];
                tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
                {
                    switch(result)
                    {
                            //  This means the user cancelled without sending the Tweet
                        case SLComposeViewControllerResultCancelled:
                        {
                            UIAlertView *alertViewCancel = [[UIAlertView alloc]
                                                            initWithTitle:@"Sorry"
                                                            message:@"Twitt is not post"
                                                            delegate:self
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil];
                            [alertViewCancel show];
                            break;
                        }
                            //  This means the user hit 'Send'
                        case SLComposeViewControllerResultDone:
                        {
                            UIAlertView *alertViewDone = [[UIAlertView alloc]
                                                          initWithTitle:@"Success"
                                                          message:@"Twitt is post"
                                                          delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                            [alertViewDone show];
                            break;
                        }
                            break;
                    }
                };
                
                
                
                [self presentViewController:tweetSheet animated:YES completion:nil];
                
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Sorry"
                                          message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                          delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                [alertView show];
            }
        }
            
            
            break;
    }
}


#pragma mark - mail composition methods
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - SMS composition methods
- (void)showSMS:(NSString*)messageText {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = [NSArray arrayWithObject:@""];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:messageText];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
