//
//  RegisterViewController.h
//  Mosque Management
//
//  Created by Developer on 17/01/14.
//  Copyright (c) 2014 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController <UITextFieldDelegate,UIAlertViewDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordField;
- (IBAction)registerButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *regBtn;
@property (strong, nonatomic) UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;

@end
